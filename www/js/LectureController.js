; // intentionally left here
(function() {
    var LectureController = Object.create(BaseController);
    Object.defineProperty(LectureController, 'treeByTabs', {enumerable: true, writable: true});
    Object.defineProperty(LectureController, '_pageName', {value: 'programme/lecture'});
    Object.defineProperty(LectureController, '_pageTitle', {value: 'Доклад'});
    Object.defineProperty(LectureController, '_lecture', {writable: true});
    Object.defineProperty(LectureController, 'onInit', {value: function ($pageContainer, page) {
        this._lecture = Schedule.lectures[page.query.lectureID];

        var $content = $pageContainer.find('.content-block').html('');
        var $preloader = $$('<span />');
        $preloader.addClass('preloader').css('width', '50px').css('height', '50px').html($$('#tpl-preloader').html());
        $preloader.appendTo($content);

        setTimeout((function () {
            var $collection = this._buildDOM();
            $collection.appendTo($content);
            $preloader.remove();
        }).bind(this), 1500);

        this.on('click', '.card', (function (e) {
            console.log('this:');
            console.log(this);
            console.log(e);

            var $card = $$(e.target);
            while (!$card.is('.card')) {
                $card = $card.parent();
            }

            console.log('Card clicked:');
            console.log($card);

            this.onCardClick($card);
        }).bind(this));

        this.on('click', '.btnAddEvent', (function (e) {
            e.preventDefault();

            var $pageContent = $$('#' + this._pageID).find('.content-block');
            var $btnAddEvent = $pageContent.find('.btnAddEvent');
            var $btnRemoveEvent = $pageContent.find('.btnRemoveEvent');

            this.addEvent($btnAddEvent, $btnRemoveEvent);
        }).bind(this));

        this.on('click', '.btnRemoveEvent', (function (e) {
            e.preventDefault();

            var $pageContent = $$('#' + this._pageID).find('.content-block');
            var $btnAddEvent = $pageContent.find('.btnAddEvent');
            var $btnRemoveEvent = $pageContent.find('.btnRemoveEvent');

            this.removeEvent($btnAddEvent, $btnRemoveEvent);
        }).bind(this));
    }});
    Object.defineProperty(LectureController, 'onAfterAnimation', {value: function (page) {
        var $navbarRight = app.$getNavbar($$('#' + this._pageID)).find('.right');
        var $btnBack = $$('<a href="#" class="link back" />');
        $btnBack.html(_('back')).appendTo($navbarRight);
        myApp.sizeNavbars('.view-main');

        this.onBackButton = function() {
            $btnBack.click();
            return false;
        };
    }});
    Object.defineProperty(LectureController, 'onCardClick', {
        value: (function ($card) {
            switch ($card.attr('data-role')) {
                case 'speaker':
                    SpeakerController().load({
                        speakerID: parseInt($card.attr('data-speaker-id'))
                    });
                    break;
                default:
                    myApp.alert('Непонятная карточка!');
                    break;
            }
        }).bind(LectureController)
    });

    Object.defineProperty(LectureController, '_buildDOMForSpeakerCard', {value: function($card, speaker) {
        $card.addClass('card speaker').attr('data-role', 'speaker').attr('data-speaker-id', speaker.id);

        var $cardContent = $$('<div />');
        $cardContent.addClass('card-content');

        var $cardContentInner = $$('<div />');
        $cardContentInner.addClass('card-content-inner');

        var photoSrc = '';
        if (typeof speaker.photoName !== typeof undefined) {
            photoSrc = cordova.file.cacheDirectory + 'unpacked/' + speaker.photoName;
        } else {
            if (speaker.sex === 'female') {
                photoSrc = 'img/female_100.png';
            } else {
                photoSrc = 'img/male_100.png';
            }
        }

        var $photo = $$('<img />');
        $photo
            .attr('src', photoSrc)
            .css('margin-right', '20px')
            .appendTo($cardContentInner);

        var $span = $$('<span />');
        $span.html(speaker['name_' + language]).appendTo($cardContentInner);

        $cardContentInner.appendTo($cardContent);
        $cardContent.appendTo($card);

        var $cardFooter = $$('<div />');
        $cardFooter.addClass('card-footer');
        var $roles = $$('<span />');
        $roles.appendTo($cardFooter);
        var $details = $$('<div />');
        $details.html(_('in details')).addClass('details').appendTo($cardFooter);
        $cardFooter.appendTo($card);

        return $card;
    }});

    Object.defineProperty(LectureController, '_buildDOM', {value: function() {
        var $collection = $$();
        var $list = null;
        var $ul = null;
        var $card, $title, $cardContent, $cardContentInner, $cardFooter, $roles, $cardHeader, $time, $details;

        // Lecture Title
        var $h1 = $$('<h1 />');
        $h1.html(this._lecture['title_' + language]);
        $collection.add($h1);

        // Lecture Time
        var $h2 = $$('<h2 />');
        $h2.html(
            _("date[format]", {
                day: this._lecture.start.getDate().toString(),
                month: _("date[month][" + this._lecture.start.getMonth() + "][full]")
            })
            + ', '
            + this._lecture.start.getHours() + ':' + this._lecture.start.getMinutes().toString().paddingLeft("00")
            + ' – '
            + this._lecture.end.getHours() + ':' + this._lecture.end.getMinutes().toString().paddingLeft("00")
        );
        $collection.add($h2);

        // Hall
        if (this._lecture.hall) {
            var $h3 = $$('<h3 />');
            $h3.html(this._lecture.hall['title_' + language]);
            $collection.add($h3);
        }

        // My Agenda
        var $myAgendaAdd = $$('<a />');
        $myAgendaAdd.addClass('button button-raised').attr('href', '#');
        $myAgendaAdd.html(_('add to my agenda')).addClass('btnAddEvent');
        $collection.add($myAgendaAdd);

        var $myAgendaRemove = $$('<a />');
        $myAgendaRemove.addClass('button button-raised').attr('href', '#');
        $myAgendaRemove.html(_('remove from my agenda')).addClass('btnRemoveEvent');
        $collection.add($myAgendaRemove);

        if (MyAgenda.isIn(this._lecture)) {
            $myAgendaAdd.hide();
        } else {
            $myAgendaRemove.hide();
        }


        // Speakers

        if (this._lecture.speakers.length) {
            $title = $$('<div />');
            $title.addClass('content-block-title').html(_('speakers'));
            $collection.add($title);

            $list = $$('<div />');
            $list.addClass('list-block cards-list');
            $ul = $$('<ul />');
            this._lecture.speakers.forEach((function (speaker) {
                $card = $$('<li />');

                this._buildDOMForSpeakerCard($card, speaker).appendTo($ul);
            }).bind(this));
            $ul.appendTo($list);
            $collection.add($list);
        }


        return $collection;
    }});

    Object.defineProperty(LectureController, 'addEvent', {value: function ($btnAddEvent, $btnRemoveEvent) {
        window.plugins.spinnerDialog.show(_('please wait'), _('adding event'), true);
        MyAgenda.addEvent(this._lecture);
        $btnAddEvent.hide();
        $btnRemoveEvent.show();
        window.plugins.spinnerDialog.hide();
    }});

    Object.defineProperty(LectureController, 'removeEvent', {value: function ($btnAddEvent, $btnRemoveEvent) {
        window.plugins.spinnerDialog.show(_('please wait'), _('removing event'), true);
        MyAgenda.removeEvent(this._lecture);
        $btnRemoveEvent.hide();
        $btnAddEvent.show();
        window.plugins.spinnerDialog.hide();
    }});

    window.LectureController = function () {
        return LectureController._construct();
    };
})();
