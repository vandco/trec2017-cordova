; // intentionally left here
(function() {
    var CustomPageController = Object.create(BaseController);
    Object.defineProperty(CustomPageController, '_pageName', {value: 'customPage'});
    Object.defineProperty(CustomPageController, '_page', {writable: true});
    Object.defineProperty(CustomPageController, '_pageTitle', {writable: true});
    Object.defineProperty(CustomPageController, 'onInit', {value: function ($pageContainer, page) {
        this._page = window.infoPages[page.query.pageID];
        $pageContainer.find('.content-block').html(this._page['content_' + language]);
        this._pageTitle = this._page['title_' + language];

        this.on('click', '.file-open', function(e) {
            e.preventDefault();
            var $target = $$(e.target);

            var onError = function (e) {
                console.log('Error status: ' + e.status + ' - Error message: ' + e.message);
                myApp.alert(_('pdf open error'));
            };

            var fileName = $target.attr('href');
            var mimeType = $target.attr('data-mime');
            window.resolveLocalFileSystemURL(cordova.file.cacheDirectory + 'unpacked/large-res/' + fileName, function(unpackedFileEntry) {
                if (isAndroid) {
                    window.resolveLocalFileSystemURL(cordova.file.externalDataDirectory, function(dirEntry) {
                        unpackedFileEntry.copyTo(dirEntry, fileName, function(newFileEntry) {
                            cordova.plugins.fileOpener2.open(
                                newFileEntry.nativeURL,
                                mimeType,
                                {error: onError}
                            );
                        }, onError);
                    });
                } else if (isIos) {
                    cordova.plugins.fileOpener2.open(
                        decodeURI(unpackedFileEntry.nativeURL),
                        mimeType,
                        {error: onError}
                    );
                }
            }, onError);

            return false;
        });

        this.on('click', 'a[data-href]', function (e) {
            e.preventDefault();

            var url = $$(this).attr('data-href');

            cordova.plugins.browsertab.isAvailable(function(result) {
                    if (!result) {
                        window.open(url, "_system");
                    } else {
                        cordova.plugins.browsertab.openUrl(
                            url,
                            function(successResp) {},
                            function(failureResp) {
                                window.open(url, "_system");
                            }
                        );
                    }
                },
                function(isAvailableError) {
                    window.open(url, "_system");
                });

            return false;
        });
    }});
    Object.defineProperty(CustomPageController, 'onAfterAnimation', {value: function (page) {
        app.setTitle(this._pageTitle);
        var $navbar = app.$getNavbar($$('#' + this._pageID));
        var $navbarRight = $navbar.find('.right');
        //var $btnBack = $$('<a href="#" class="link back" />');
        //$btnBack.html(_('back')).appendTo($navbarRight);
        myApp.sizeNavbars('.view-main');

        /*this.onBackButton = function() {
            $btnBack.click();
            return false;
        };*/
    }});


    window.CustomPageController = function () {
        return CustomPageController._construct();
    };
})();
