; // intentionally left here
(function() {
    var SpeakersController = Object.create(BaseController);
    Object.defineProperty(SpeakersController, '_pageName', {value: 'programme/speakers'});
    Object.defineProperty(SpeakersController, '_pageTitle', {value: 'Спикеры'});
    Object.defineProperty(SpeakersController, 'onInit', {value: function ($pageContainer, page) {
        var $content = $pageContainer.find('.content-block').html('');
        var $preloader = $$('<span />');
        $preloader.addClass('preloader').css('width', '50px').css('height', '50px').html($$('#tpl-preloader').html());
        $preloader.appendTo($content);

        setTimeout((function () {
            var $collection = this._buildDOM();
            var $searchCollection = this._buildSearchDOM();

            $searchCollection.appendTo($pageContainer);
            $collection.appendTo($content);

            myApp.searchbar('#' + this._pageID + ' .searchbar', {
                searchList: $pageContainer.find('.list-block-search'),
                searchIn: '#' + this._pageID + ' .card-content'
            });
            $preloader.remove();
        }).bind(this), 1500);

        this.on('click', '.card', (function (e) {
            console.log('this:');
            console.log(this);
            console.log(e);

            var $card = $$(e.target);
            while (!$card.is('.card')) {
                $card = $card.parent();
            }

            console.log('Card clicked:');
            console.log($card);

            this.onCardClick($card);
        }).bind(this));
    }});
    Object.defineProperty(SpeakersController, 'onAfterAnimation', {value: function (page) {
        var $navbarRight = app.$getNavbar($$('#' + this._pageID)).find('.right');
        //var $btnBack = $$('<a href="#" class="link back" />');
        //$btnBack.html(_('back')).appendTo($navbarRight);
        myApp.sizeNavbars('.view-main');

        /*this.onBackButton = function() {
            $btnBack.click();
            return false;
        };*/
    }});
    Object.defineProperty(SpeakersController, 'onCardClick', {
        value: (function ($card) {
            SpeakerController().load({
                speakerID: parseInt($card.attr('data-speaker-id'))
            });
        }).bind(SpeakersController)
    });

    Object.defineProperty(SpeakersController, '_buildDOMForSpeakerCard', {value: function($card, speaker, rolesStr) {
        $card.addClass('card speaker').attr('data-role', 'speaker').attr('data-speaker-id', speaker.id);

        var $cardContent = $$('<div />');
        $cardContent.addClass('card-content');

        var $cardContentInner = $$('<div />');
        $cardContentInner.addClass('card-content-inner');

        var photoSrc = '';
        if (typeof speaker.photoName !== typeof undefined) {
            photoSrc = cordova.file.cacheDirectory + 'unpacked/' + speaker.photoName;
        } else {
            if (speaker.sex === 'female') {
                photoSrc = 'img/female_100.png';
            } else {
                photoSrc = 'img/male_100.png';
            }
        }

        var $photo = $$('<img />');
        $photo
            .attr('src', photoSrc)
            .css('margin-right', '20px')
            .appendTo($cardContentInner);

        var $span = $$('<span />');
        $span.html(speaker['name_' + language]).appendTo($cardContentInner);

        $cardContentInner.appendTo($cardContent);
        $cardContent.appendTo($card);

        var $cardFooter = $$('<div />');
        $cardFooter.addClass('card-footer');
        var $roles = $$('<span />');
        $roles.addClass('hall').html(rolesStr).appendTo($cardFooter);
        var $details = $$('<div />');
        $details.html(_('in details')).addClass('details').appendTo($cardFooter);
        $cardFooter.appendTo($card);

        return $card;
    }});

    Object.defineProperty(SpeakersController, '_buildDOM', {value: function() {
        var $collection = $$();
        var $list = null;
        var $ul = null;
        var $card, $title, $cardContent, $cardContentInner, $cardFooter, $roles, $cardHeader, $time;
        var roles;

        // All speakers
        $list = $$('<div />');
        $list.addClass('list-block list-block-search cards-list searchbar-found');
        $ul = $$('<ul />');

        var speakers = [];
        var speaker;
        for (var speakerID in Schedule.speakers) {
            speaker = Schedule.speakers[speakerID];
            speakers.push(speaker);
        }

        var fieldName = 'name_' + language;
        speakers.sort(function(a, b) {
            if (a[fieldName] >  b[fieldName]) return 1;
            if (a[fieldName] == b[fieldName]) return 0;
            if (a[fieldName] <  b[fieldName]) return -1;
        });

        speakers.forEach((function(speaker) {
            $card = $$('<li />');
            roles = [];

            if (speaker._sessionsAsChairman.length) {
                roles.push(_('chairman'));
            }
            if (speaker._sessionsAsNarrator.length) {
                roles.push(_('narrator'));
            }
            if (speaker._lectures.length) {
                roles.push(_('speaker'));
            }

            this._buildDOMForSpeakerCard($card, speaker, roles.join(', ')).appendTo($ul);
        }).bind(this));

        $ul.appendTo($list);
        $collection.add($list);

        return $collection;
    }});

    Object.defineProperty(SpeakersController, '_buildSearchDOM', {value: function() {
        return $$([
            '<form class="searchbar">',
            '   <div class="searchbar-input">',
            '       <input type="search" placeholder="' + _('search') + '">',
            '       <a href="#" class="searchbar-clear"></a>',
            '   </div>',
            '   <a href="#" class="searchbar-cancel">' + _('cancel') + '</a>',
            '</form>',
            '<div class="searchbar-overlay"></div>'
        ].join('\n'));
    }});

        window.SpeakersController = function () {
        return SpeakersController._construct();
    };
})();
