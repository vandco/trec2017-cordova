; // intentionally left here
(function() {
    var correctTimezoneOffset = -3 * 60;

    var sessions = Object.create(null);
    var lectures = Object.create(null);
    var speakers = Object.create(null);
    var halls    = Object.create(null);
    var colours    = Object.create(null);

    // 'Singleton' Schedule (always links to the same collections)
    var schedule = Object.create(null);
    Object.defineProperty(schedule, 'sessions', {
        get: function() { return sessions; },
        enumerable: true
    });
    Object.defineProperty(schedule, '_sessions_cached', {writable: true});
    Object.defineProperty(schedule, 'sortedSessions', {enumerable: true, get: function () {
        if (!this._sessions_cached) {
            this._sessions_cached = [];
            var heap = [];

            for (var sessionID in sessions) {
                // No need to filter by .hasOwnProperty() because no inheritance
                heap.push(sessions[sessionID]);
            }

            console.log('Sorting sessions');
            heap.sort(
                firstBy(function (a, b) {
                    if (a.start.getTime() > b.start.getTime()) return 1;
                    if (a.start.getTime() == b.start.getTime()) return 0;
                    if (a.start.getTime() < b.start.getTime()) return -1;
                }).thenBy(function(a, b) {
                    if (a.end.getTime() > b.end.getTime()) return 1;
                    if (a.end.getTime() == b.end.getTime()) return 0;
                    if (a.end.getTime() < b.end.getTime()) return -1;
                }).thenBy(function(a, b) {
                    if (a.title_ru > b.title_ru) return 1;
                    if (a.title_ru == b.title_ru) return 0;
                    if (a.title_ru < b.title_ru) return -1;
                })
            );

            heap.forEach((function (session) {
                this._sessions_cached.push(session);
            }).bind(this));
        }

        return this._sessions_cached;
    }});
    Object.defineProperty(schedule, 'lectures', {
        get: function() { return lectures; },
        enumerable: true
    });
    Object.defineProperty(schedule, 'speakers', {
        get: function() { return speakers; },
        enumerable: true
    });
    Object.defineProperty(schedule, 'halls', {
        get: function() { return halls; },
        enumerable: true
    });
    Object.defineProperty(schedule, 'colours', {
        get: function() { return colours; },
        enumerable: true
    });

    // Session 'class'
    var Session = {};
    Object.defineProperty(Session, 'id', {writable: true, enumerable: true});
    Object.defineProperty(Session, 'title_ru', {writable: true, enumerable: true});
    Object.defineProperty(Session, 'title_en', {writable: true, enumerable: true});
    Object.defineProperty(Session, 'is_caption', {writable: true, enumerable: true});
    Object.defineProperty(Session, 'start', {writable: true, enumerable: true});
    Object.defineProperty(Session, '_hall', {writable: true, value: null});
    Object.defineProperty(Session, 'hall', {enumerable: true, get: function () {
        return this._hall;
    }});
    Object.defineProperty(Session, 'setHall', {enumerable: true, value: function (hall) {
        hall.insertSession(this);
    }});
    Object.defineProperty(Session, '_colour', {writable: true, value: null});
    Object.defineProperty(Session, 'colour', {enumerable: true, get: function () {
        return this._colour;
    }});
    Object.defineProperty(Session, 'setColour', {enumerable: true, value: function (colour) {
        colour.insertSession(this);
    }});
    Object.defineProperty(Session, '_lectures_heap', {writable: true, value: []});
    Object.defineProperty(Session, '_lectures_cached', {writable: true});
    Object.defineProperty(Session, 'lectures', {enumerable: true, get: function () {
        if (!this._lectures_cached) {
            this._lectures_cached = [];

            this._lectures_heap.sort(function (a, b) {
                if (a.order > b.order) return 1;
                if (a.order == b.order) return 0;
                if (a.order < b.order) return -1;
            });

            var timeCounter = new Date(this.start.getTime());
            this._lectures_heap.forEach((function (lectureObj) {
                var start = new Date(timeCounter.getTime());
                var end = new Date(start.getTime() + lectureObj.lecture.duration * 60 * 1000);
                timeCounter = new Date(end.getTime());

                this._lectures_cached.push({
                    lecture: lectureObj.lecture,
                    start: start,
                    end: end
                });
            }).bind(this));

            this._lectures_cached.sort(
                firstBy(function (a, b) {
                    if (a.start.getTime() > b.start.getTime()) return 1;
                    if (a.start.getTime() == b.start.getTime()) return 0;
                    if (a.start.getTime() < b.start.getTime()) return -1;
                }).thenBy(function(a, b) {
                    if (a.end.getTime() > b.end.getTime()) return 1;
                    if (a.end.getTime() == b.end.getTime()) return 0;
                    if (a.end.getTime() < b.end.getTime()) return -1;
                })
            );
        }

        return this._lectures_cached;
    }});
    Object.defineProperty(Session, 'insertLecture', {enumerable: true, value: function (lecture, order) {
        this._lectures_cached = null;

        lecture._sessions.push(this);
        //var lectStart = new Date(this._end.getTime());
        //var lectEnd = new Date(lectStart.getTime() + lecture.duration * 60 * 1000);
        this._end = new Date(this._end.getTime() + lecture.duration * 60 * 1000);

        this._lectures_heap.push({
            lecture: lecture,
            //start: lectStart,
            //end: lectEnd
            order: order
        });
    }});
    Object.defineProperty(Session, '_end', {writable: true});
    Object.defineProperty(Session, 'end', {enumerable: true, get: function () {
        return this._end;
    }});
    Object.defineProperty(Session, '_chairmen_heap', {writable: true, value: []});
    Object.defineProperty(Session, '_chairmen_cached', {writable: true});
    Object.defineProperty(Session, 'chairmen', {enumerable: true, get: function () {
        if (!this._chairmen_cached) {
            this._chairmen_cached = [];

            this._chairmen_heap.sort(function (a, b) {
                if (a.order > b.order) return 1;
                if (a.order == b.order) return 0;
                if (a.order < b.order) return -1;
            });

            this._chairmen_heap.forEach((function (speakerObj) {
                this._chairmen_cached.push(speakerObj.speaker);
            }).bind(this));
        }

        return this._chairmen_cached;
    }});
    Object.defineProperty(Session, 'insertChairman', {enumerable: true, value: function (speaker, order) {
        this._chairmen_cached = null;

        speaker._sessionsAsChairman.push(this);

        this._chairmen_heap.push({
            speaker: speaker,
            order: order
        });
    }});
    Object.defineProperty(Session, '_narrators_heap', {writable: true, value: []});
    Object.defineProperty(Session, '_narrators_cached', {writable: true});
    Object.defineProperty(Session, 'narrators', {enumerable: true, get: function () {
        if (!this._narrators_cached) {
            this._narrators_cached = [];

            var fieldName = 'name_' + language;
            this._narrators_heap.sort(function(a, b) {
                if (a.speaker[fieldName] > b.speaker[fieldName]) return 1;
                if (a.speaker[fieldName] == b.speaker[fieldName]) return 0;
                if (a.speaker[fieldName] < b.speaker[fieldName]) return -1;
            });

            this._narrators_heap.forEach((function (speakerObj) {
                this._narrators_cached.push(speakerObj.speaker);
            }).bind(this));
        }

        return this._narrators_cached;
    }});
    Object.defineProperty(Session, 'insertNarrator', {enumerable: true, value: function (speaker, order) {
        this._narrators_cached = null;

        speaker._sessionsAsNarrator.push(this);

        this._narrators_heap.push({
            speaker: speaker,
            order: order
        });
    }});
    Object.defineProperty(Session, 'toString', {enumerable: true, value: function() {
        return '[object Session]';
    }});

    // Lecture 'class'
    var Lecture = {};
    Object.defineProperty(Lecture, 'id', {writable: true, enumerable: true});
    Object.defineProperty(Lecture, 'title_ru', {writable: true, enumerable: true});
    Object.defineProperty(Lecture, 'title_en', {writable: true, enumerable: true});
    Object.defineProperty(Lecture, 'is_caption', {writable: true, enumerable: true});
    Object.defineProperty(Lecture, 'duration', {writable: true, enumerable: true});
    Object.defineProperty(Lecture, '_sessions', {writable: true, value: []});
    Object.defineProperty(Lecture, 'sessions', {enumerable: true, get: function () {
        return this._sessions;
    }});
    Object.defineProperty(Lecture, 'appendToSession', {enumerable: true, value: function (session, order) {
        session.insertLecture(this, order);
    }});
    Object.defineProperty(Lecture, '_start_cached', {writable: true, value: null});
    Object.defineProperty(Lecture, 'start', {enumerable: true, get: function () {
        if (this._start_cached === null) {
            if (this.sessions.length != 1) {
                return null;
            }

            var boundSession = this.sessions[0];
            boundSession.lectures.every((function (lectureObj) {
                if (lectureObj.lecture === this) {
                    this._start_cached = lectureObj.start;
                    return false;
                }
                return true;
            }).bind(this));
        }

        return this._start_cached;
    }});
    Object.defineProperty(Lecture, '_end_cached', {writable: true, value: null});
    Object.defineProperty(Lecture, 'end', {enumerable: true, get: function () {
        if (this._end_cached === null) {
            this._end_cached = new Date(this.start.getTime() + this.duration * 60 * 1000);
        }

        return this._end_cached;
    }});
    Object.defineProperty(Lecture, '_hall_cached', {writable: true, value: null});
    Object.defineProperty(Lecture, 'hall', {enumerable: true, get: function () {
        if (this._hall_cached === null) {
            if (this.sessions.length != 1) {
                return null;
            }

            var boundSession = this.sessions[0];
            this._hall_cached = boundSession.hall;
        }

        return this._hall_cached;
    }});
    Object.defineProperty(Lecture, '_speakers_heap', {writable: true, value: []});
    Object.defineProperty(Lecture, '_speakers_cached', {writable: true});
    Object.defineProperty(Lecture, 'speakers', {enumerable: true, get: function () {
        if (!this._speakers_cached) {
            this._speakers_cached = [];

            this._speakers_heap.sort(function (a, b) {
                if (a.order > b.order) return 1;
                if (a.order == b.order) return 0;
                if (a.order < b.order) return -1;
            });

            this._speakers_heap.forEach((function (speakerObj) {
                this._speakers_cached.push(speakerObj.speaker);
            }).bind(this));
        }

        return this._speakers_cached;
    }});
    Object.defineProperty(Lecture, 'insertSpeaker', {enumerable: true, value: function (speaker, order) {
        this._speakers_cached = null;

        speaker._lectures.push(this);

        this._speakers_heap.push({
            speaker: speaker,
            order: order
        });
    }});
    Object.defineProperty(Lecture, 'toString', {enumerable: true, value: function() {
        return '[object Lecture]';
    }});

    // Speaker 'class'
    var Speaker = {};
    Object.defineProperty(Speaker, 'id', {writable: true, enumerable: true});
    Object.defineProperty(Speaker, 'name_ru', {writable: true, enumerable: true});
    Object.defineProperty(Speaker, 'name_en', {writable: true, enumerable: true});
    Object.defineProperty(Speaker, 'lastName_ru', {writable: true, enumerable: true});
    Object.defineProperty(Speaker, 'lastName_en', {writable: true, enumerable: true});
    Object.defineProperty(Speaker, 'firstName_ru', {writable: true, enumerable: true});
    Object.defineProperty(Speaker, 'firstName_en', {writable: true, enumerable: true});
    Object.defineProperty(Speaker, 'photoName', {writable: true, enumerable: true});
    Object.defineProperty(Speaker, 'sex', {writable: true, enumerable: true});
    Object.defineProperty(Speaker, '_sessionsAsChairman', {writable: true, value: []});
    Object.defineProperty(Speaker, 'sessionsAsChairman', {enumerable: true, get: function () {
        return this._sessionsAsChairman;
    }});
    Object.defineProperty(Speaker, 'appendToSessionAsChairman', {enumerable: true, value: function (session, order) {
        session.insertChairman(this, order);
    }});
    Object.defineProperty(Speaker, '_sessionsAsNarrator', {writable: true, value: []});
    Object.defineProperty(Speaker, 'sessionsAsNarrator', {enumerable: true, get: function () {
        return this._sessionsAsNarrator;
    }});
    Object.defineProperty(Speaker, 'appendToSessionAsNarrator', {enumerable: true, value: function (session, order) {
        session.insertNarrator(this, order);
    }});
    Object.defineProperty(Speaker, '_lectures', {writable: true, value: []});
    Object.defineProperty(Speaker, 'lectures', {enumerable: true, get: function () {
        return this._lectures;
    }});
    Object.defineProperty(Speaker, 'appendToLecture', {enumerable: true, value: function (lecture, order) {
        lecture.insertSpeaker(this, order);
    }});
    Object.defineProperty(Speaker, 'toString', {enumerable: true, value: function() {
        return '[object Speaker]';
    }});

    // Hall 'class'
    var Hall = {};
    Object.defineProperty(Hall, 'id', {writable: true, enumerable: true});
    Object.defineProperty(Hall, 'title_ru', {writable: true, enumerable: true});
    Object.defineProperty(Hall, 'title_en', {writable: true, enumerable: true});
    Object.defineProperty(Hall, 'color', {writable: true, enumerable: true});
    Object.defineProperty(Hall, '_sessions_heap', {writable: true, value: []});
    Object.defineProperty(Hall, '_sessions_cached', {writable: true});
    Object.defineProperty(Hall, 'sessions', {enumerable: true, get: function () {
        if (!this._sessions_cached) {
            this._sessions_cached = [];

            this._sessions_heap.forEach((function (session) {
                this._sessions_cached.push(session);
            }).bind(this));

            this._sessions_cached.sort(
                firstBy(function (a, b) {
                    if (a.start.getTime() > b.start.getTime()) return 1;
                    if (a.start.getTime() == b.start.getTime()) return 0;
                    if (a.start.getTime() < b.start.getTime()) return -1;
                }).thenBy(function(a, b) {
                    if (a.end.getTime() > b.end.getTime()) return 1;
                    if (a.end.getTime() == b.end.getTime()) return 0;
                    if (a.end.getTime() < b.end.getTime()) return -1;
                })
            );
        }

        return this._sessions_cached;
    }});
    Object.defineProperty(Hall, 'insertSession', {enumerable: true, value: function (session) {
        this._sessions_cached = null;
        session._hall = this;
        this._sessions_heap.push(session);
    }});
    Object.defineProperty(Hall, 'assignToSession', {enumerable: true, value: function (session) {
        session.setHall(this);
    }});
    Object.defineProperty(Hall, 'toString', {enumerable: true, value: function() {
        return '[object Hall]';
    }});

    // Colour 'class'
    var Colour = {};
    Object.defineProperty(Colour, 'id', {writable: true, enumerable: true});
    Object.defineProperty(Colour, 'title_ru', {writable: true, enumerable: true});
    Object.defineProperty(Colour, 'title_en', {writable: true, enumerable: true});
    Object.defineProperty(Colour, 'bgCssColor', {writable: true, enumerable: true});
    Object.defineProperty(Colour, 'circleCssColor', {writable: true, enumerable: true});
    Object.defineProperty(Colour, '_sessions_heap', {writable: true, value: []});
    Object.defineProperty(Colour, '_sessions_cached', {writable: true});
    Object.defineProperty(Colour, 'sessions', {enumerable: true, get: function () {
        if (!this._sessions_cached) {
            this._sessions_cached = [];

            this._sessions_heap.forEach((function (session) {
                this._sessions_cached.push(session);
            }).bind(this));

            this._sessions_cached.sort(
                firstBy(function (a, b) {
                    if (a.start.getTime() > b.start.getTime()) return 1;
                    if (a.start.getTime() == b.start.getTime()) return 0;
                    if (a.start.getTime() < b.start.getTime()) return -1;
                }).thenBy(function(a, b) {
                    if (a.end.getTime() > b.end.getTime()) return 1;
                    if (a.end.getTime() == b.end.getTime()) return 0;
                    if (a.end.getTime() < b.end.getTime()) return -1;
                })
            );
        }

        return this._sessions_cached;
    }});
    Object.defineProperty(Colour, 'insertSession', {enumerable: true, value: function (session) {
        this._sessions_cached = null;
        session._colour = this;
        this._sessions_heap.push(session);
    }});
    Object.defineProperty(Colour, 'toString', {enumerable: true, value: function() {
        return '[object Colour]';
    }});

    // Schedule manipulation methods
    var addSession = function (sessionData) {
        var id = parseInt(sessionData['id']);
        if (id in sessions) {
            return false;
        }

        var session = Object.create(Session);
        session.id = id;
        session.title_ru = sessionData['title_ru'];
        session.title_en = sessionData['title_en'];
        session.is_caption = !!parseInt(sessionData['is_caption']);
        session.start = new Date(
            parseInt(sessionData['start_timestamp']) * 1000
            + 60 * 1000 * (localTimezoneOffset - correctTimezoneOffset)
        );
        session._end = new Date(
            parseInt(sessionData['start_timestamp']) * 1000
            + 60 * 1000 * (localTimezoneOffset - correctTimezoneOffset)
        );

        if (sessionData['hall_id'] !== null) {
            session.hall_id = parseInt(sessionData['hall_id']);
            session.setHall(halls[session.hall_id]);
        }

        if (sessionData['colour_id'] !== null) {
            session.colour_id = parseInt(sessionData['colour_id']);
            session.setColour(colours[session.colour_id]);
        }

        Object.defineProperty(session, '_lectures_heap', {writable: true, value: []});
        Object.defineProperty(session, '_lectures_cached', {writable: true, value: null});

        Object.defineProperty(session, '_chairmen_heap', {writable: true, value: []});
        Object.defineProperty(session, '_chairmen_cached', {writable: true, value: null});

        Object.defineProperty(session, '_narrators_heap', {writable: true, value: []});
        Object.defineProperty(session, '_narrators_cached', {writable: true, value: null});

        schedule._sessions_cached = null;

        sessions[id] = session;
        return true;
    };
    Object.defineProperty(schedule, 'addSession', {
        value: addSession,
        enumerable: true
    });

    var addLecture = function (lectureData) {
        var id = parseInt(lectureData['id']);
        if (id in lectures) {
            return false;
        }

        var lecture = Object.create(Lecture);
        lecture.id = id;
        lecture.title_ru = lectureData['title_ru'];
        lecture.title_en = lectureData['title_en'];
        lecture.is_caption = !!parseInt(lectureData['is_caption']);
        lecture.duration = parseInt(lectureData['duration']);

        Object.defineProperty(lecture, '_start_cached', {writable: true, value: null});
        Object.defineProperty(lecture, '_end_cached', {writable: true, value: null});

        Object.defineProperty(lecture, '_hall_cached', {writable: true, value: null});

        Object.defineProperty(lecture, '_speakers_heap', {writable: true, value: []});
        Object.defineProperty(lecture, '_speakers_cached', {writable: true, value: null});

        Object.defineProperty(lecture, '_sessions', {writable: true, value: []});

        lectures[id] = lecture;
        return true;
    };
    Object.defineProperty(schedule, 'addLecture', {
        value: addLecture,
        enumerable: true
    });

    var addSpeaker = function (speakerData) {
        var id = parseInt(speakerData['id']);
        if (id in speakers) {
            return false;
        }

        var speaker = Object.create(Speaker);
        speaker.id = id;
        speaker.name_ru = speakerData['name_ru'];
        speaker.name_en = speakerData['name_en'];
        speaker.lastName_ru = speakerData['lastName_ru'];
        speaker.lastName_en = speakerData['lastName_en'];
        speaker.firstName_ru = speakerData['firstName_ru'];
        speaker.firstName_en = speakerData['firstName_en'];
        speaker.photoName = speakerData['photoName'];
        speaker.sex = speakerData['sex'];

        Object.defineProperty(speaker, '_sessionsAsChairman', {writable: true, value: []});
        Object.defineProperty(speaker, '_sessionsAsNarrator', {writable: true, value: []});
        Object.defineProperty(speaker, '_lectures', {writable: true, value: []});

        speakers[id] = speaker;
        return true;
    };
    Object.defineProperty(schedule, 'addSpeaker', {
        value: addSpeaker,
        enumerable: true
    });

    var addHall = function (hallData) {
        var id = parseInt(hallData['id']);
        if (id in halls) {
            return false;
        }

        var hall = Object.create(Hall);
        hall.id = id;
        hall.title_ru = hallData['title_ru'];
        hall.title_en = hallData['title_en'];
        hall.color = hallData['color'];

        Object.defineProperty(hall, '_sessions_heap', {writable: true, value: []});
        Object.defineProperty(hall, '_sessions_cached', {writable: true, value: null});

        halls[id] = hall;
        return true;
    };
    Object.defineProperty(schedule, 'addHall', {
        value: addHall,
        enumerable: true
    });

    var addColour = function (colourData) {
        var id = parseInt(colourData['id']);
        if (id in colours) {
            return false;
        }

        var colour = Object.create(Colour);
        colour.id = id;
        colour.title_ru = colourData['title_ru'];
        colour.title_en = colourData['title_en'];
        colour.bgCssColor = colourData['bgCssColor'];
        colour.circleCssColor = colourData['circleCssColor'];

        Object.defineProperty(colour, '_sessions_heap', {writable: true, value: []});
        Object.defineProperty(colour, '_sessions_cached', {writable: true, value: null});

        colours[id] = colour;
        return true;
    };
    Object.defineProperty(schedule, 'addColour', {
        value: addColour,
        enumerable: true
    });

    // Process parsed JSON
    var initSchedule = function (scheduleData, filteringCallbacks, onProgress, onComplete) {
        console.log('Schedule Data:');
        console.log(scheduleData);

        var ok = true;

        sessions = Object.create(null);
        lectures = Object.create(null);
        speakers = Object.create(null);
        halls    = Object.create(null);
        colours  = Object.create(null);

        var total =
            scheduleData['halls'].length
            + scheduleData['colours'].length
            + scheduleData['speakers'].length
            + scheduleData['sessions'].length
            + scheduleData['lectures'].length
            + scheduleData['lectures2session'].length
            + scheduleData['chairmen2session'].length
            + scheduleData['narrators2session'].length
            + scheduleData['speakers2lectures'].length;
        var loaded = 0;

        var incrementProgress = function(value) {
            if (!value) value = 1;
            loaded += value;
            if (onProgress) {
                onProgress(new ProgressEvent('progress', {lengthComputable: true, loaded: loaded, total: total}));
            }
        };
        
        var idx;
        
        var addHalls, addColours, addSpeakers, addSessions, addLectures, bindLecturesToSessions, bindChairmenToSessions, bindNarratorsToSessions, bindSpeakersToLectures;
        var filteredHalls = {}, filteredColours = {}, filteredSpeakers = {}, filteredSessions = {}, filteredLectures = {};
        var filterSessions;

        addHalls = function() {
            scheduleData['halls'].forEach(function (hallData) {
                console.log('Adding ' + (idx+1) + ' of ' + scheduleData['halls'].length);
                if (!schedule.addHall(hallData)) {
                    console.error('Could not add hall:');
                    console.log(hallData);
                    ok = false;
                } else if ('halls' in filteringCallbacks) {
                    var hall = halls[parseInt(hallData['id'])];
                    for (var filteringKey in filteringCallbacks.halls) {
                        if (!filteringCallbacks.halls.hasOwnProperty(filteringKey))
                            continue;
                        if (filteringCallbacks.halls[filteringKey](hall)) {
                            if (!(filteringKey in filteredHalls)) {
                                filteredHalls[filteringKey] = [];
                            }
                            filteredHalls[filteringKey].push(hall);
                        }
                    }
                }
            });
            incrementProgress(scheduleData['halls'].length);
            idx += scheduleData['halls'].length;
            
            console.groupEnd();
            console.groupCollapsed('Adding colours');
            idx = 0;
            setTimeout(addColours, 0);
        };

        addColours = function() {
            scheduleData['colours'].forEach(function (colourData) {
                console.log('Adding ' + (idx+1) + ' of ' + scheduleData['colours'].length);
                if (!schedule.addColour(colourData)) {
                    console.error('Could not add colour:');
                    console.log(colourData);
                    ok = false;
                } else if ('colours' in filteringCallbacks) {
                    var colour = colours[parseInt(colourData['id'])];
                    for (var filteringKey in filteringCallbacks.colours) {
                        if (!filteringCallbacks.colours.hasOwnProperty(filteringKey))
                            continue;
                        if (filteringCallbacks.colours[filteringKey](colour)) {
                            if (!(filteringKey in filteredColours)) {
                                filteredColours[filteringKey] = [];
                            }
                            filteredColours[filteringKey].push(colour);
                        }
                    }
                }
            });
            incrementProgress(scheduleData['colours'].length);
            idx += scheduleData['colours'].length;

            console.groupEnd();
            console.groupCollapsed('Adding speakers');
            idx = 0;
            setTimeout(addSpeakers, 0);
        };

        addSpeakers = function() {
            scheduleData['speakers'].forEach(function (speakerData) {
                console.log('Adding ' + (idx+1) + ' of ' + scheduleData['speakers'].length);
                if (!schedule.addSpeaker(speakerData)) {
                    console.error('Could not add speaker:');
                    console.log(speakerData);
                    ok = false;
                } else if ('speakers' in filteringCallbacks) {
                    var speaker = speakers[parseInt(speakerData['id'])];
                    for (var filteringKey in filteringCallbacks.speakers) {
                        if (!filteringCallbacks.speakers.hasOwnProperty(filteringKey))
                            continue;
                        if (filteringCallbacks.speakers[filteringKey](speaker)) {
                            if (!(filteringKey in filteredSpeakers)) {
                                filteredSpeakers[filteringKey] = [];
                            }
                            filteredSpeakers[filteringKey].push(speaker);
                        }
                    }
                }
            });
            incrementProgress(scheduleData['speakers'].length);
            idx += scheduleData['speakers'].length;

            console.groupEnd();
            console.groupCollapsed('Adding sessions');
            idx = 0;
            setTimeout(addSessions, 0);
        };

        addSessions = function() {
            scheduleData['sessions'].forEach(function (sessionData) {
                console.log('Adding ' + (idx+1) + ' of ' + scheduleData['sessions'].length);
                if (!schedule.addSession(sessionData)) {
                    console.error('Could not add session:');
                    console.log(sessionData);
                    ok = false;
                }
            });
            incrementProgress(scheduleData['sessions'].length);
            idx += scheduleData['sessions'].length;

            console.groupEnd();
            console.groupCollapsed('Adding lectures');
            idx = 0;
            setTimeout(addLectures, 0);
        };

        addLectures = function() {
            scheduleData['lectures'].forEach(function (lectureData) {
                console.log('Adding ' + (idx+1) + ' of ' + scheduleData['lectures'].length);
                if (!schedule.addLecture(lectureData)) {
                    console.error('Could not add lecture:');
                    console.log(lectureData);
                    ok = false;
                } else if ('lectures' in filteringCallbacks) {
                    var lecture = lectures[parseInt(lectureData['id'])];
                    for (var filteringKey in filteringCallbacks.lectures) {
                        if (!filteringCallbacks.lectures.hasOwnProperty(filteringKey))
                            continue;
                        if (filteringCallbacks.lectures[filteringKey](lecture)) {
                            if (!(filteringKey in filteredLectures)) {
                                filteredLectures[filteringKey] = [];
                            }
                            filteredLectures[filteringKey].push(lecture);
                        }
                    }
                }
            });
            incrementProgress(scheduleData['lectures'].length);
            idx += scheduleData['lectures'].length;

            console.groupEnd();
            console.groupCollapsed('Binding lectures to sessions');
            idx = 0;
            setTimeout(bindLecturesToSessions, 0);
        };

        bindLecturesToSessions = function() {
            scheduleData['lectures2session'].every(function (lectures2SessionsData) {
                console.log('Binding ' + (idx+1) + ' of ' + scheduleData['lectures2session'].length);

                var lecture = lectures[lectures2SessionsData['lectureId']];
                if (!lecture) {
                    console.error('Could not find lecture ID: ' + lectures2SessionsData['lectureId']);
                    ok = false;
                    return true;
                }

                var session = sessions[lectures2SessionsData['sessionId']];
                if (!session) {
                    console.error('Could not find session ID: ' + lectures2SessionsData['sessionId']);
                    ok = false;
                    return true;
                }

                var order = parseInt(sessions[lectures2SessionsData['order']]);
                lecture.appendToSession(session, order);

                console.log('New end of session ID ' + session.id + ':');
                console.log(session.end);

                return true;
            });
            incrementProgress(scheduleData['lectures2session'].length);
            idx += scheduleData['lectures2session'].length;

            console.groupEnd();
            console.groupCollapsed('Binding chairmen to sessions');
            idx = 0;
            setTimeout(bindChairmenToSessions, 0);
        };

        bindChairmenToSessions = function() {
            scheduleData['chairmen2session'].every(function (chairmen2SessionsData) {
                console.log('Binding ' + (idx+1) + ' of ' + scheduleData['chairmen2session'].length);

                var speaker = speakers[chairmen2SessionsData['speakerId']];
                if (!speaker) {
                    console.error('Could not find speaker ID: ' + chairmen2SessionsData['speakerId']);
                    ok = false;
                    return true;
                }

                var session = sessions[chairmen2SessionsData['sessionId']];
                if (!session) {
                    console.error('Could not find session ID: ' + chairmen2SessionsData['sessionId']);
                    ok = false;
                    return true;
                }

                var order = parseInt(sessions[chairmen2SessionsData['order']]);
                speaker.appendToSessionAsChairman(session, order);

                return true;
            });
            incrementProgress(scheduleData['chairmen2session'].length);
            idx += scheduleData['chairmen2session'].length;

            console.groupEnd();
            console.groupCollapsed('Binding narrators to sessions');
            idx = 0;
            setTimeout(bindNarratorsToSessions, 0);
        };

        bindNarratorsToSessions = function() {
            scheduleData['narrators2session'].every(function (narrators2SessionsData) {
                console.log('Binding ' + (idx+1) + ' of ' + scheduleData['narrators2session'].length);

                var speaker = speakers[narrators2SessionsData['speakerId']];
                if (!speaker) {
                    console.error('Could not find speaker ID: ' + narrators2SessionsData['speakerId']);
                    ok = false;
                    return true;
                }

                var session = sessions[narrators2SessionsData['sessionId']];
                if (!session) {
                    console.error('Could not find session ID: ' + narrators2SessionsData['sessionId']);
                    ok = false;
                    return true;
                }

                var order = parseInt(sessions[narrators2SessionsData['order']]);
                speaker.appendToSessionAsNarrator(session, order);

                return true;
            });
            incrementProgress(scheduleData['narrators2session'].length);
            idx += scheduleData['narrators2session'].length;

            console.groupEnd();
            console.groupCollapsed('Binding speakers to lectures');
            idx = 0;
            setTimeout(bindSpeakersToLectures, 0);
        };

        bindSpeakersToLectures = function() {
            scheduleData['speakers2lectures'].every(function (speakers2LecturesData) {
                console.log('Binding ' + (idx+1) + ' of ' + scheduleData['speakers2lectures'].length);

                var speaker = speakers[speakers2LecturesData['speakerId']];
                if (!speaker) {
                    console.error('Could not find speaker ID: ' + speakers2LecturesData['speakerId']);
                    ok = false;
                    return true;
                }

                var lecture = lectures[speakers2LecturesData['lectureId']];
                if (!lecture) {
                    console.error('Could not find lecture ID: ' + speakers2LecturesData['lectureId']);
                    ok = false;
                    return true;
                }

                var order = parseInt(sessions[speakers2LecturesData['order']]);
                speaker.appendToLecture(lecture, order);

                return true;
            });
            incrementProgress(scheduleData['speakers2lectures'].length);
            idx += scheduleData['speakers2lectures'].length;

            console.groupEnd();
            filterSessions();
            setTimeout(function () {
                onComplete(ok, {
                    halls: filteredHalls,
                    speakers: filteredSpeakers,
                    sessions: filteredSessions,
                    lectures: filteredLectures
                });
            }, 0);
        };

        filterSessions = function () {
            console.log('Filtering sessions');
            if ('sessions' in filteringCallbacks) {
                schedule.sortedSessions.forEach(function (session) {
                    for (var filteringKey in filteringCallbacks.sessions) {
                        if (!filteringCallbacks.sessions.hasOwnProperty(filteringKey))
                            continue;
                        if (filteringCallbacks.sessions[filteringKey](session)) {
                            if (!(filteringKey in filteredSessions)) {
                                filteredSessions[filteringKey] = [];
                            }
                            filteredSessions[filteringKey].push(session);
                        }
                    }
                });
            }
        };

        console.groupCollapsed('Adding halls');
        idx = 0;
        setTimeout(addHalls, 0);
    };
    Object.defineProperty(schedule, 'init', {
        value: initSchedule,
        enumerable: true
    });

    window.Schedule = schedule;
})();
