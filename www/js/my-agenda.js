; // intentionally left here
(function() {
    // 'Singleton' MyAgenda (always links to the same collections)
    var myAgenda = Object.create(null);
    Object.defineProperty(myAgenda, '_modified', {writable: true, value: false});
    Object.defineProperty(myAgenda, 'unModify', {value: function () {
        if (!this._modified) return false;
        this._modified = false; return true;
    }});
    Object.defineProperty(myAgenda, 'isModified', {value: function () { return this._modified; }});
    Object.defineProperty(myAgenda, '_on_save_handler', {writable: true, value: (function(arr) {}).bind(myAgenda)});
    Object.defineProperty(myAgenda, '_events_heap', {writable: true, value: []});
    Object.defineProperty(myAgenda, '_events_map', {writable: true, value: {
        sessions: {},
        lectures: {}
    }});
    Object.defineProperty(myAgenda, '_events_cached', {writable: true});
    Object.defineProperty(myAgenda, 'sortedEvents', {enumerable: true, get: function () {
        if (!this._events_cached) {
            this._events_cached = [];
            var heap = [];

            this._events_heap.forEach(function (event) {
                heap.push(event);
            });

            console.log('Sorting events');
            heap.sort(
                firstBy(function (a, b) {
                    if (a.start.getTime() > b.start.getTime()) return 1;
                    if (a.start.getTime() == b.start.getTime()) return 0;
                    if (a.start.getTime() < b.start.getTime()) return -1;
                }).thenBy(function(a, b) {
                    if (a.end.getTime() > b.end.getTime()) return 1;
                    if (a.end.getTime() == b.end.getTime()) return 0;
                    if (a.end.getTime() < b.end.getTime()) return -1;
                }).thenBy(function(a, b) {
                    if (a.title_ru > b.title_ru) return 1;
                    if (a.title_ru == b.title_ru) return 0;
                    if (a.title_ru < b.title_ru) return -1;
                })
            );

            heap.forEach((function (event) {
                this._events_cached.push(event);
            }).bind(this));

            this.save();
        }

        return this._events_cached;
    }});
    Object.defineProperty(myAgenda, 'save', {
        value: function () {
            var events;
            if (this._events_cached) {
                events = this.sortedEvents;
            } else {
                events = this._events_heap;
            }
            var toSave = [];
            events.forEach(function (event) {
                if (event.toString() === '[object Session]') {
                    toSave.push({
                        kind: 'session',
                        id: event.id
                    });
                } else if (event.toString() === '[object Lecture]') {
                    toSave.push({
                        kind: 'lecture',
                        id: event.id
                    });
                }
            });

            this._on_save_handler.call(this, toSave);
        },
        enumerable: true
    });


    // MyAgenda manipulation methods
    var addEvent = function (event) {
        myAgenda._events_cached = null;
        myAgenda._events_heap.push(event);

        if (event.toString() === '[object Session]') {
            myAgenda._events_map.sessions[event.id] = event;
        } else if (event.toString() === '[object Lecture]') {
            myAgenda._events_map.lectures[event.id] = event;
        }

        myAgenda._modified = true;
        myAgenda.save();
        return true;
    };
    Object.defineProperty(myAgenda, 'addEvent', {
        value: addEvent,
        enumerable: true
    });

    var removeEvent = function (event) {
        var idx = myAgenda._events_heap.indexOf(event);
        if (idx == -1) {
            return false;
        }

        myAgenda._events_heap.splice(idx, 1);
        myAgenda._events_cached = null;

        if (event.toString() === '[object Session]') {
            delete myAgenda._events_map.sessions[event.id];
        } else if (event.toString() === '[object Lecture]') {
            delete myAgenda._events_map.lectures[event.id];
        }

        myAgenda._modified = true;
        myAgenda.save();
        return true;
    };
    Object.defineProperty(myAgenda, 'removeEvent', {
        value: removeEvent,
        enumerable: true
    });

    var isIn = function (event) {
        var idx = myAgenda._events_heap.indexOf(event);
        return (idx != -1);
    };
    Object.defineProperty(myAgenda, 'isIn', {
        value: isIn,
        enumerable: true
    });


    // Process parsed JSON
    var initMyAgenda = function (myAgendaData, onSaveHandler) {
        console.log('My Agenda:');
        console.log(myAgendaData);

        myAgenda._on_save_handler = onSaveHandler;

        myAgendaData.forEach(function (eventObj) {
            if (eventObj.kind === 'session') {
                if (eventObj.id in Schedule.sessions) {
                    myAgenda.addEvent(Schedule.sessions[eventObj.id]);
                } else {
                    console.log('FAIL: no event found for MyAgenda: Session ID ' + eventObj.id);
                }
            } else if (eventObj.kind === 'lecture') {
                if (eventObj.id in Schedule.lectures) {
                    myAgenda.addEvent(Schedule.lectures[eventObj.id]);
                } else {
                    console.log('FAIL: no event found for MyAgenda: Lecture ID ' + eventObj.id);
                }
            }
        });
    };
    Object.defineProperty(myAgenda, 'init', {
        value: initMyAgenda,
        enumerable: true
    });


    window.MyAgenda = myAgenda;
})();
