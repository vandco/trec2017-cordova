; // intentionally left here
(function() {
    var NewsSingleController = Object.create(BaseController);
    Object.defineProperty(NewsSingleController, '_pageName', {value: 'news-single'});
    Object.defineProperty(NewsSingleController, '_pageTitle', {value: 'Новость'});
    //Object.defineProperty(NewsController, '_page', {writable: true});
    Object.defineProperty(NewsSingleController, 'onInit', {value: function ($pageContainer, page) {
        var context = {
            language: language,
            newsObj: window.news[page.query.newsID]
        };
        var compiledHtml = window.compiledTpls.newsSingle(context).trim();
        $pageContainer.find('.content-block').html(compiledHtml);
    }});
    Object.defineProperty(NewsSingleController, 'onAfterAnimation', {value: function (page) {
        var $navbarRight = app.$getNavbar($$('#' + this._pageID)).find('.right');
        var $btnBack = $$('<a href="#" class="link back" />');
        $btnBack.html(_('back')).appendTo($navbarRight);
        myApp.sizeNavbars('.view-main');

        this.onBackButton = function() {
            $btnBack.click();
            return false;
        };
    }});


    window.NewsSingleController = function () {
        return NewsSingleController._construct();
    };
})();
