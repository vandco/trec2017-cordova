; // intentionally left here
(function() {
    var QuestionsController = Object.create(BaseController);
    Object.defineProperty(QuestionsController, '_pageName', {value: 'questions'});
    Object.defineProperty(QuestionsController, '_pageTitle', {value: 'Задать вопрос'});
    Object.defineProperty(QuestionsController, 'onInit', {value: function ($pageContainer, page) {
        var contacts = localStorage.getItem('contacts');
        if (contacts !== null) {
            try {
                contacts = JSON.parse(contacts);
            } catch(e) {
                localStorage.removeItem('contacts');
                contacts = null;
            }
        }

        var $contentBlock = $pageContainer.find('.content-block');

        var $fromName = $pageContainer.find('[name="fromName"]');
        var $fromEmail = $pageContainer.find('[name="fromEmail"]');
        var $textArea = $pageContainer.find('textarea');
        var $btnSubmit = $contentBlock.find('button[type="submit"]');

        if (contacts !== null) {
            if (typeof contacts.name !== typeof undefined) {
                $fromName.val(contacts.name);
            }

            if (typeof contacts.email !== typeof undefined) {
                $fromEmail.val(contacts.email);
            }
        }

        this.on('click', 'button[type="submit"]', (function(e) {
            e.preventDefault();

            $$('.has-error').removeClass('has-error');

            if (
                !$fromName.val().trim().length
                || !$textArea.val().trim().length
            ) {
                myApp.alert(_('required fields empty'));

                if (!$fromName.val().trim().length) {
                    $fromName.parent().parent().addClass('has-error');
                }

                if (!$textArea.val().trim().length) {
                    $textArea.parent().parent().addClass('has-error');
                }

                return;
            }

            var $preloader = $$('<span />');
            $preloader.addClass('preloader').css('width', '50px').css('height', '50px').html($$('#tpl-preloader').html());
            $preloader.insertAfter($btnSubmit);
            $btnSubmit.hide();
            window.plugins.spinnerDialog.show(_('sending question'), _('please wait'), true);

            this.submitQuestion({
                fromName: $fromName.val(),
                fromEmail: $fromEmail.val(),
                question: $textArea.val()
            }, function() {
                myApp.alert(_('asked ok'));

                contacts = {
                    name: $fromName.val(),
                    email: $fromEmail.val()
                };
                localStorage.setItem('contacts', JSON.stringify(contacts));

                $preloader.remove();
                $btnSubmit.show();
                window.plugins.spinnerDialog.hide();
            }, function(data) {
                var decodedErrors;
                try {
                    decodedErrors = JSON.parse(data);
                    var errorMsg = _('ask fields errors') + ' <br />\n<br />\n';
                    var errorFields = [];
                    decodedErrors.forEach(function(errObj) {
                        for (var fieldCode in errObj) {
                            var errorField = '';
                            if (fieldCode === 'fromName') {
                                errorField = '<b>' + _('your name') + '</b>: ';
                            }
                            if (fieldCode === 'fromEmail') {
                                errorField = '<b>' + _('your email') + '</b>: ';
                            }
                            if (fieldCode === 'question') {
                                errorField = '<b>' + _('your question here') + '</b>: ';
                            }

                            errorField += errObj[fieldCode];
                            errorFields.push(errorField);
                        }
                    });

                    errorMsg += errorFields.join(' <br />\n');
                    myApp.alert(errorMsg);
                } catch(e) {
                    myApp.alert(_('ask failed'));
                }

                $preloader.remove();
                $btnSubmit.show();
                window.plugins.spinnerDialog.hide();
            }, function() {
                myApp.alert(_('ask error'));
                $preloader.remove();
                $btnSubmit.show();
                window.plugins.spinnerDialog.hide();
            });
        }).bind(this));
    }});
    Object.defineProperty(QuestionsController, 'onAfterAnimation', {value: function (page) {
        var $navbarRight = app.$getNavbar($$('#' + this._pageID)).find('.right');
        //var $btnBack = $$('<a href="#" class="link back" />');
        //$btnBack.html(_('back')).appendTo($navbarRight);
        myApp.sizeNavbars('.view-main');

        /*this.onBackButton = function() {
            $btnBack.click();
            return false;
        };*/
    }});

    Object.defineProperty(QuestionsController, 'submitQuestion', {value: function(data, callbackOnSuccess, callbackOnError, callbackOnConnectivityFailure) {
        $$.ajax({
            url: 'https://trec-course.ru/mobile-app/askQuestion',
            //crossDomain: true,
            dataType: 'json',
            timeout: 5000,
            method: 'post',
            data: data,
            success: function(data) {
                console.log(data);
                callbackOnSuccess();
            },
            error: function (xhr, status) {
                console.log(xhr);
                console.log(status);
                if (status === 'timeout') {
                    callbackOnConnectivityFailure();
                } else {
                    callbackOnError(xhr.responseText);
                }
            }
        });
    }});


    window.QuestionsController = function () {
        return QuestionsController._construct();
    };
})();
