; // intentionally left here
(function() {
    var MyAgendaController = Object.create(BaseController);
    Object.defineProperty(MyAgendaController, '_pageName', {value: 'my-agenda'});
    Object.defineProperty(MyAgendaController, '_pageTitle', {value: 'Моё расписание'});
    Object.defineProperty(MyAgendaController, '_$pageContainer', {writable: true});
    Object.defineProperty(MyAgendaController, 'onInit', {value: function ($pageContainer, page) {
        this._$pageContainer = $pageContainer;

        $pageContainer.find('.subnavbar .buttons-row a').each((function (idx, elem) {
            $$(elem).attr('href', '#tab' + idx + '-' + this._pageID);
        }).bind(this));
        $pageContainer.find('.tab').each((function (idx, elem) {
            $$(elem).attr('id', 'tab' + idx + '-' + this._pageID);
        }).bind(this));

        this.on('show', '.tab', (function (e) {
            var $tab = $$(e.target);
            var tabIdx = $tab.index();
            console.log($tab);
            console.log(tabIdx);
            this.onTabShow(tabIdx);
        }).bind(this));

        this.on('click', '.card', (function (e) {
            console.log('this:');
            console.log(this);
            console.log(e);

            var $card = $$(e.target);
            while (!$card.is('.card')) {
                $card = $card.parent();
            }

            console.log('Card clicked:');
            console.log($card);

            this.onCardClick($card);
        }).bind(this));

        this._rebuildDOM();
    }});
    Object.defineProperty(MyAgendaController, 'onAfterAnimation', {value: function (page) {
        if (MyAgenda.isModified()) {
            this._rebuildDOM();
        }

        var $navbarRight = app.$getNavbar($$('#' + this._pageID)).find('.right');
        //var $btnBack = $$('<a href="#" class="link back" />');
        //$btnBack.html(_('back')).appendTo($navbarRight);
        myApp.sizeNavbars('.view-main');

        /*this.onBackButton = function() {
            $btnBack.click();
            return false;
        };*/
    }});

    Object.defineProperty(MyAgendaController, 'onTabShow', {
        value: (function (tabIdx) {
            console.log('My Agenda for tab:');
            var $tab = $$('#tab' + tabIdx + '-' + this._pageID);
        }).bind(MyAgendaController)
    });
    Object.defineProperty(MyAgendaController, 'onCardClick', {
        value: (function ($card) {
            switch ($card.attr('data-role')) {
                case 'session':
                    SessionController().load({
                        sessionID: parseInt($card.attr('data-session-id'))
                    });
                    break;
                case 'lecture':
                    LectureController().load({
                        lectureID: parseInt($card.attr('data-lecture-id'))
                    });
                    break;
                default:
                    myApp.alert('Непонятная карточка!');
                    break;
            }
        }).bind(MyAgendaController)
    });

    Object.defineProperty(MyAgendaController, '_rebuildDOM', {value: function() {
        MyAgenda.unModify();

        var $tabs = this._$pageContainer.find('.page-content.tab').find('.content-block');
        var $preloaders = $$();

        $tabs.each(function () {
            var $tab = $$(this);
            $tab.html('');
            var $preloader = $$('<span />');
            $preloader.addClass('preloader').css('width', '50px').css('height', '50px').html($$('#tpl-preloader').html());
            $preloader.appendTo($tab);
            $preloaders = $preloaders.add($preloader);
        });

        setTimeout((function () {
            this._buildDOM();
            $preloaders.remove();
        }).bind(this), 500);
    }});

    Object.defineProperty(MyAgendaController, '_buildDOM', {value: function() {
        var listsByDays = {};
        var startDateStrKey;

        var $ul, $list;
        var $card, $sessionCard, $title, $sessionTitle, $sessionContent, $sessionContentInner, chairmenArr, narratorsArr, $p, $sessionFooter, $details, $hall, $cardContent, $cardContentInner, $cardFooter, $roles, $cardHeader, $time, $session;


        // Events

        MyAgenda.sortedEvents.forEach((function (event) {
            if (event.toString() === '[object Session]') {
                if (!event['title_' + language]) {
                    return;
                }

                startDateStrKey = new Date(event.start);
                startDateStrKey.setHours(0);
                startDateStrKey.setMinutes(0);
                startDateStrKey.setSeconds(0);
                startDateStrKey.setMilliseconds(0);
                startDateStrKey = _("date[format]", {
                    day: startDateStrKey.getDate().toString(),
                    month: _("date[month][" + startDateStrKey.getMonth() + "][full]")
                });

                if (!(startDateStrKey in listsByDays)) {
                    $list = $$('<div />');
                    $list.addClass('list-block cards-list');
                    $ul = $$('<ul />');
                    $ul.appendTo($list);
                    listsByDays[startDateStrKey] = $list;
                } else {
                    $ul = listsByDays[startDateStrKey].find('ul');
                }

                var context = {
                    language: language,
                    date: true,
                    session: event
                };

                var compiledHtml = window.compiledTpls.sessionCard(context).trim();
                $sessionCard = $$(compiledHtml);

                $sessionCard.appendTo($ul);
            } else if (event.toString() === '[object Lecture]') {
                if (event.sessions.length != 1) {
                    return true;
                }

                startDateStrKey = new Date(event.sessions[0].start);
                startDateStrKey.setHours(0);
                startDateStrKey.setMinutes(0);
                startDateStrKey.setSeconds(0);
                startDateStrKey.setMilliseconds(0);
                startDateStrKey = _("date[format]", {
                    day: startDateStrKey.getDate().toString(),
                    month: _("date[month][" + startDateStrKey.getMonth() + "][full]")
                });

                if (!(startDateStrKey in listsByDays)) {
                    $list = $$('<div />');
                    $list.addClass('list-block cards-list');
                    $ul = $$('<ul />');
                    $ul.appendTo($list);
                    listsByDays[startDateStrKey] = $list;
                } else {
                    $ul = listsByDays[startDateStrKey].find('ul');
                }

                context = {
                    language: language,
                    lecture: event,
                    date: true
                };
                compiledHtml = window.compiledTpls.lectureCard(context).trim();
                $card = $$(compiledHtml);

                $card.appendTo($ul);
            }
        }).bind(this));


        var dateStr;
        var $tabs = this._$pageContainer.find('.tab.page-content');
        var $tab;
        for (dateStr in listsByDays) {
            $tab = $tabs.filter(function() {
                return ($$(this).attr('data-date-str') == dateStr);
            });
            $tab.addClass(dateStr.replace(/[\s|,]/g, '-'));

            var $content = $tab.find('.content-block');
            listsByDays[dateStr].appendTo($content);
        }

        $tabs.each(function () {
            if (!($$(this).attr('data-date-str') in listsByDays)) {
                $p = $$('<p />');
                $p.css('text-align', 'center').html(_('no events in my agenda'));
                var $content = $$(this).find('.content-block');
                $p.appendTo($content);
            }
        });
    }});

    window.MyAgendaController = function () {
        return MyAgendaController._construct();
    };
})();
