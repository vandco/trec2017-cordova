var myApp;
var mainView;
var language;
var style = 'ios';
var isGod;
var $$ = Dom7;
var isAndroid = Framework7.prototype.device.android === true;
var isIos = Framework7.prototype.device.ios === true;
var lockedBackgroundVerification = false;
var localTimezoneOffset = (new Date()).getTimezoneOffset();

if (isAndroid) {
    style = 'material';
}

var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        var thisPtr = this;

        if (isAndroid) {
            var exitAppModalShown = false;
            document.addEventListener("backbutton", function (e) {
                e.preventDefault();
                console.log('Back Button pressed');
                if ($$('.panel-left').hasClass('active')) {
                    myApp.closePanel(true);
                } else {
                    if (
                        (typeof BaseController._controllersStack === typeof undefined)
                        || (!BaseController._controllersStack.length)
                        || (BaseController._controllersStack.slice(-1)[0].controller.onBackButton())
                    ) {
                        if (!exitAppModalShown) {
                            myApp.confirm(_('exit app text'), _('exit app title'), function () {
                                navigator.app.exitApp();
                            }, function () {
                                exitAppModalShown = false;
                            });
                            exitAppModalShown = true;
                        }
                    }
                }
            }, false);

            navigator.app.overrideButton("menubutton", true);  // Workaround Cordova bug
            document.addEventListener("menubutton", function (e) {
                e.preventDefault();
                console.log('Menu Button pressed');
                if (
                    (typeof BaseController._controllersStack === typeof undefined)
                    || (!BaseController._controllersStack.length)
                    || (BaseController._controllersStack.slice(-1)[0].controller.onMenuButton())
                ) {
                    if ($$('.panel-left').hasClass('active')) {
                        myApp.closePanel(true);
                    } else {
                        myApp.openPanel('left', true);
                    }
                }
            }, false);
        } else if (isIos) {
            if (typeof window.Keyboard !== typeof undefined) {
                window.Keyboard.shrinkView(true);
            }
        }

        // правильная работа с часовыми поясами
        document.addEventListener('resume', function () {
            if ((new Date()).getTimezoneOffset() !== localTimezoneOffset) {
                app.restart();
            }
        });

        // поддержка тестового режима
        isGod = localStorage.getItem("god");
        if (isGod) {
            isGod = (isGod === 'true');
        } else {
            isGod = false;
            localStorage.setItem("god", 'false');
        }

        // news last seen timestamp
        if (!localStorage.getItem('newsLastSeenTimestamp')) {
            localStorage.setItem('newsLastSeenTimestamp', 0);
        }

        // при первой загрузке загружаем запомненный язык
        language = localStorage.getItem("language");
        if (language) {
            console.log('User Chosen Language: ' + language);
            app.onLanguageReady.call(thisPtr);
        } else {
            navigator.globalization.getPreferredLanguage(
                function (lang) {
                    language = lang.value.substr(0, 2);
                    console.log('Platform Language: ' + language);
                    app.onLanguageReady.call(thisPtr);
                },
                function () {
                    console.error('Failed to get current language!');
                    language = 'ru-RU';
                    app.onLanguageReady.call(thisPtr);
                }
            );
        }
    },

    onLanguageReady: function () {
        var thisPtr = this;

        switch (language) {
            //case 'en':
            case 'ru':
                break;
            default:
                language = 'ru';
        }

        html10n.localize(language);
        html10n.bind('localized', function () {
            html10n.unbind('localized');
            document.documentElement.lang = html10n.getLanguage();
            document.documentElement.dir  = html10n.getDirection();
            app.initializeApp.call(thisPtr);
        });
    },

    initializeApp: function() {
        var thisPtr = this;

        Handlebars.registerHelper('html10n', function (str, opts) {
            return (html10n != undefined) ? html10n.get(str) : str;
        });

        Handlebars.registerHelper('date', function (timestamp, opts) {
            var date = new Date(timestamp * 1000);
            return html10n.get('date[format]', {
                day: date.getDate().toString(),
                month: html10n.get('date[month][' + date.getMonth() + '][full]'),
                year: date.getFullYear(),
                hours: date.getHours().toString().paddingLeft("00"),
                minutes: date.getMinutes().toString().paddingLeft("00"),
                seconds: date.getSeconds().toString().paddingLeft("00")
            });
        });

        Handlebars.registerHelper('if_eq', function (a, b, opts) {
            if (a == b) {
                return opts.fn(this);
            } else {
                return opts.inverse(this);
            }
        });

        Handlebars.registerHelper('if_eq_strict', function (a, b, opts) {
            if (a === b) {
                return opts.fn(this);
            } else {
                return opts.inverse(this);
            }
        });

        Handlebars.registerHelper('join', function (val, params) {
            return [].concat(val).join(params.hash.delimiter);
        });
        Handlebars.registerHelper('speakers', function (arr, curr) {
            var tmp = [];
            arr.forEach(function (speaker) {
                var speakerName = speaker['name_' + language];
                if (speaker === curr) {
                    tmp.push('<strong>' + speakerName + '</strong>');
                } else {
                    tmp.push(speakerName);
                }
            });
            return tmp.join(', ');
        });
        Handlebars.registerHelper('localized_prop', function (params) {
            return params.hash.object[params.hash.propName + '_' + language];
        });
        Handlebars.registerHelper('format_time', function (date) {
            return date.getHours() + ':' + date.getMinutes().toString().paddingLeft("00");
        });
        Handlebars.registerHelper('format_date', function (date) {
            return _("date[format]", {
                day: date.getDate().toString(),
                month: _("date[month][" + date.getMonth() + "][full]")
            });
        });

        // Initialize app
        myApp = new Framework7({
            // Enable Material theme for Android device only
            material: !!isAndroid,
            // Enable Template7 pages
            template7Pages: false,
            materialRipple: false,

            animateNavBackIcon: true,
            pushState: true, //при переходе между экранами, чтобы работала кнопка back на android
            modalTitle: "ТРЭК 2018",
            modalButtonCancel: "Отмена", //текст Cancel кнопки
            swipePanel: 'left', //включаем левого меню свайпом
            swipeBackPage: false
        });

        var changeStyle = function (styleName) {
            $$("#pageStyle").attr("href", 'lib/framework7/css/framework7.' + styleName + '.min.css');
            $$("#pageStyleColours").attr("href", 'lib/framework7/css/framework7.' + styleName + '.colors.min.css');

            var $pages = $$('.page');

            if (styleName === 'material') {
                isAndroid = true;
                isIos = false;

                $$.each($pages, function (idx, elem) {
                    var $page = $$(elem);
                    if (idx == 0) {
                        $page.prepend($$('.navbar'));
                    } else {
                        app.cloneBarsToPage($page);
                    }
                });

                $$('.pages')
                    .addClass('navbar-fixed')
                    .removeClass('navbar-through');
            } else if (styleName === 'ios') {
                isAndroid = false;
                isIos = true;

                var $subnavbar = $$('.page.page-on-center').find('.subnavbar');
                if ($subnavbar.length) {
                    $$($$('.navbar')[0]).append($subnavbar);
                }

                $$('.navbar:not([id])').remove();

                $$('.view').prepend($$('.navbar'));

                $$('.pages')
                    .removeClass('navbar-fixed')
                    .addClass('navbar-through');
            }

            style = styleName;

            $$('body').attr('class', styleName);
        };

        var applyLanguage = function () {
            var context = {
                language: language,
                style: style
            };

            var navbarTpl = Handlebars.compile($$('#navbar-tpl').html());
            $$('#navbar-tpl').parent().html(navbarTpl(context));

            var splashTpl = Handlebars.compile($$('#splash-tpl').html());
            $$('#splash-tpl').parent().html(splashTpl(context));
        };

        // при первой загрузке загружаем запомненный css
        if (localStorage.getItem("css")) {
            changeStyle(localStorage.getItem("css"));
        } else {
            changeStyle(style);
        }

        applyLanguage(language);

        $$('body').css('display', '');

        window.plugins.spinnerDialog.hide();

        thisPtr.compileHandlebarsTemplates();
    },

    compileHandlebarsTemplates: function () {
        window.compiledTpls = {};

        var templates = [
            cordova.file.applicationDirectory + 'www/programme/templates/sessionCard.hbs',
            cordova.file.applicationDirectory + 'www/programme/templates/lectureCard.hbs',
            cordova.file.applicationDirectory + 'www/templates/news.hbs',
            cordova.file.applicationDirectory + 'www/templates/newsSingle.hbs'
        ];
        
        var promise = templates.reduce(function (prevValue, curItem) {
            return prevValue.then(function () {
                return new Promise(function (resolve) {
                    var winKey = /\/([\w\d\-_]+)\.hbs/g.exec(curItem)[1];
                    app.compileTpl(curItem, winKey, resolve);
                });
            })
        }, Promise.resolve());

        promise.then(function () {
            app.startApp();
        });
    },

    compileTpl: function (fileName, winKey, callback) {
        window.resolveLocalFileSystemURL(fileName, function (tplFileEntry) {
            tplFileEntry.file(function (tplFile) {
                var fileReader = new FileReader();
                fileReader.onloadend = function (controller) {
                    console.log(this.result);
                    window.compiledTpls[winKey] = Handlebars.compile(this.result);
                    callback();
                };
                fileReader.readAsText(tplFile);
            });
        });
    },

    startApp: function () {
        var $statusText = $$('.login-screen').find('.status-text');

        var verifyTimestamp, onNetError, onNetErrorIgnored, onDownloaded, onExists, onNotFound, onNotUnpacked, onUnzipped, readJSON, onError, onProgress, unzipBundled;
        var cacheDirEntry, dataDirEntry, unpackedDirEntry, zipFileEntry;

        onProgress = function (progressEvent) {
            myApp.setProgressbar('.login-screen .progressbar', progressEvent.loaded / progressEvent.total * 100);
        };
        onError = function(msg) {
            console.log('Error:');
            console.log(msg);
            myApp.alert('Всё плохо:\n\n' + msg);
        };
        onNetError = function (callbackRetry, callbackIgnore, zipEntry) {
            $$('.login-screen .btnCancel-container').hide();

            myApp.modal({
                title: _('net error title'),
                text: _('net error text'),
                buttons: [
                    {
                        text: _('retry'),
                        close: true,
                        onClick: callbackRetry.bind(null, zipEntry)
                    },
                    {
                        text: _('ignore'),
                        close: true,
                        onClick: callbackIgnore.bind(null, zipEntry)
                    }
                ]
            });
        };

        var oldUnpackedDirBackedUp = false;

        onDownloaded = function (entry) {
            $$('.login-screen .btnCancel-container').hide();

            console.log("Downloaded: " + entry.toURL());

            var onUnpackedDirBackedUp = function () {
                onExists(entry);
            };

            cacheDirEntry.getDirectory('unpacked', {}, function(dirEntry) {
                console.log('Backing old unpacked dir up for a case ZIP is corrupted');
                dirEntry.moveTo(cacheDirEntry, 'unpacked.old', function() {
                    oldUnpackedDirBackedUp = true;
                    onUnpackedDirBackedUp();
                });
            }, onUnpackedDirBackedUp);
        };

        unzipBundled = function (zipEntry) {
            // fallback to bundled ZIP
            $statusText.text(_('preparing first'));
            $$('.login-screen .progressbar-infinite').show();
            $$('.login-screen .progressbar').hide();

            window.resolveLocalFileSystemURL(cordova.file.applicationDirectory, function (appDirEntry) {
                appDirEntry.getDirectory('www', {}, function (wwwDirEntry) {
                    wwwDirEntry.getDirectory('res', {}, function (resDirEntry) {
                        resDirEntry.getFile('conf-1.zip', {exclusive: false}, function (bundledZipFileEntry) {
                            bundledZipFileEntry.copyTo(zipEntry.filesystem.root, 'conf.zip', onDownloaded.bind(null, zipEntry), onError);
                        }, onError);
                    })
                });
            }, onError);
        };

        onNetErrorIgnored = function (zipEntry) {
            if ('packVer' in localStorage) {
                onExists(zipEntry);
            } else {
                unzipBundled(zipEntry);
            }
        };

        onExists = function (zipEntry) {
            zipFileEntry = zipEntry;
            cacheDirEntry.getDirectory('unpacked', {}, function() {
                onUnzipped(0);
            }, onNotUnpacked);
        };

        onNotFound = function () {
            dataDirEntry.getFile('conf.zip', {
                create: true,
                exclusive: false
            }, function (fileEntry) {
                console.log('Downloading to: ' + fileEntry.toURL());
                var srcUrl = 'https://schedule.vandco.ru/upload/conf-1.zip';
                console.log('Downloading from: ' + srcUrl);

                var fileTransfer = new FileTransfer();
                var fileURL = fileEntry.toURL();

                fileTransfer.onprogress = onProgress;
                $statusText.text(_('downloading'));
                fileTransfer.download(
                    srcUrl,
                    fileURL,
                    onDownloaded,
                    function() { onNetError(onNotFound, onNetErrorIgnored, fileEntry); }
                );

                $$('.login-screen .btnCancel-container').show();
                $$('.login-screen .btnCancel').on('click', function (e) {
                    fileTransfer.abort();
                });
            }, onError);
        };

        onNotUnpacked = function () {
            console.log('Data:');
            console.log(dataDirEntry);
            cacheDirEntry.getDirectory('unpacked', {
                create: true
            }, function (dirEntry) {
                unpackedDirEntry = dirEntry;
                $$('.login-screen .progressbar-infinite').hide();
                $$('.login-screen .progressbar').show();
                $statusText.text(_('unpacking'));
                window.zip.unzip(zipFileEntry.toURL(), unpackedDirEntry.toURL(), onUnzipped, onProgress);
            }, onError);
        };

        onUnzipped = function (status) {
            if (status == 0) {
                console.log('Unzipped: ', cordova.file.cacheDirectory);
                if (oldUnpackedDirBackedUp) {
                    console.log('Deleting old unzipped dir');
                    cacheDirEntry.getDirectory('unpacked.old', {}, function(oldDirEntry) {
                        oldDirEntry.removeRecursively();
                    });
                }

                $$('.login-screen .progressbar-infinite').hide();
                $$('.login-screen .progressbar').show();
                $statusText.text(_('processing'));

                cacheDirEntry.getDirectory('unpacked', {}, function (dirEntry) {
                    unpackedDirEntry = dirEntry;
                    readJSON();
                }, onError);
            } else {
                var notifyUser = function (wasBackup) {
                    if (wasBackup) {
                        onNetError(onNotFound, onUnzipped.bind(null, 0));
                    } else {
                        dataDirEntry.getFile('conf.zip', {
                            create: true,
                            exclusive: false
                        }, function (fileEntry) {
                            onNetError(onNotFound, unzipBundled, fileEntry);
                        });
                    }
                };

                if (oldUnpackedDirBackedUp) {
                    console.log('Deleting new failed unpacked dir');

                    var restore = function () {
                        console.log('Restoring backup');
                        cacheDirEntry.getDirectory('unpacked.old', {}, function (dirEntry) {
                            dirEntry.moveTo(cacheDirEntry, 'unpacked', function() {
                                oldUnpackedDirBackedUp = false;
                                notifyUser(true);
                            });
                        });
                    };

                    cacheDirEntry.getDirectory('unpacked', {}, function (dirEntry) {
                        dirEntry.removeRecursively(restore);
                    }, restore);
                } else {
                    notifyUser(false);
                }
            }
        };

        readJSON = function () {
            unpackedDirEntry.getFile('main.json', {}, function (jsonFileEntry) {
                jsonFileEntry.file(function (jsonFile) {
                    var fileReader = new FileReader();
                    fileReader.onloadend = function () {
                        console.log(this.result);
                        var schedule = JSON.parse(this.result);
                        console.log(schedule);

                        window.menuItems = schedule.dynamicContent.menu;
                        window.infoPagesMetadata = schedule.dynamicContent.infoPages;
                        window.infoPages = {};
                        window.infoPagesMetadata.forEach(function(elem) {
                            window.infoPages[elem.id] = elem;
                        });

                        window.menu = [];
                        window.menuItems.forEach(function(elem) {
                            var menuItem = Object.assign({}, elem);
                            if (elem.info_page_id !== null) {
                                menuItem['icon_mime'] = window.infoPages[elem.info_page_id].icon_mime;
                                menuItem['icon_b64'] = window.infoPages[elem.info_page_id].icon_b64;
                                menuItem['title_ru'] = window.infoPages[elem.info_page_id].title_ru;
                                menuItem['title_en'] = window.infoPages[elem.info_page_id].title_en;
                            }
                            window.menu.push(menuItem);
                        });

                        window.mapObjects = schedule.dynamicContent.mapObjects;

                        window.news = {};
                        schedule.dynamicContent.news.forEach(function(newsItem) {
                            window.news[newsItem.id] = newsItem;
                        });

                        localStorage['packVer'] = schedule.version;

                        var day1_start = new Date(2018, 1-1, 26);
                        var day1_end = new Date(2018, 1-1, 26, 23, 59, 59);
                        var day2_start = new Date(2018, 1-1, 27);
                        var day2_end = new Date(2018, 1-1, 27, 23, 59, 59);

                        Schedule.init(schedule, {
                            sessions: {
                                tab1: function(session) {
                                    return (
                                        (session.start.getTime() >= day1_start.getTime())
                                        && (session.start.getTime() <= day1_end.getTime())
                                    );
                                },
                                tab2: function(session) {
                                    return (
                                        (session.start.getTime() >= day2_start.getTime())
                                        && (session.start.getTime() <= day2_end.getTime())
                                    );
                                }
                            }
                        }, onProgress, function(ok, filteredObjects) {
                            console.log('Tabbed Schedule:');
                            console.log(filteredObjects);
                            ProgrammeController().treeByTabs = filteredObjects.sessions;

                            if (!('myAgenda' in localStorage)) {
                                localStorage['myAgenda'] = JSON.stringify([]);
                            }
                            var myAgendaData = JSON.parse(localStorage['myAgenda']);
                            var saveMyAgenda = function (toSave) {
                                localStorage['myAgenda'] = JSON.stringify(toSave);
                            };
                            MyAgenda.init(myAgendaData, saveMyAgenda);

                            app.initializeMainView.call(app);
                        });
                    };
                    fileReader.readAsText(jsonFile);
                }, onError);
            }, onError);
        };

        verifyTimestamp = function () {
            app.verifyScheduleTimestamp(
                // onNoTimestamp
                function () {
                    $$('.login-screen .progressbar-infinite').hide();
                    $$('.login-screen .progressbar').show();

                    onNotFound();
                },

                // onBeginVerifying
                function () {
                    $statusText.text(_('connecting'));
                },
                // onSuccessVerifying
                function () {
                    $$('.login-screen .progressbar-infinite').hide();
                    $$('.login-screen .progressbar').show();
                },
                // onUpdated
                function () {
                    onNotFound();
                },
                // onActual
                function () {
                    dataDirEntry.getFile('conf.zip', {}, onExists, onNotFound);
                },
                // onError
                function () {
                    console.log('AJAX Error');
                    dataDirEntry.getFile('conf.zip', {}, function (fileEntry) {
                        onNetError(verifyTimestamp, onExists, fileEntry);
                    }, onNotFound);
                }
                // onComplete
            );
        };

        window.resolveLocalFileSystemURL(cordova.file.dataDirectory, function (dirEntry) {
            dataDirEntry = dirEntry;

            window.resolveLocalFileSystemURL(cordova.file.cacheDirectory, function (dirEntry) {
                cacheDirEntry = dirEntry;
                verifyTimestamp();
            }, onError);
        }, onError);
    },

    verifyScheduleTimestamp: function(
        onNoTimestamp,
        onBeginVerifying,
        onSuccessVerifying,
        onUpdated,
        onActual,
        onError,
        onComplete
    ) {
        if ('packVer' in localStorage) {
            localStorage['lastVerifiedTimestamp'] = (new Date()).getTime();
            if (typeof onBeginVerifying === typeof (function(){})) onBeginVerifying();

            $$.ajax({
                url: 'https://schedule.vandco.ru/schedule/latestVersion/1',
                crossDomain: true,
                dataType: 'jsonp',
                timeout: 5000,
                success: function(ver) {
                    if (typeof onSuccessVerifying === typeof (function(){})) onSuccessVerifying();

                    if (ver > localStorage['packVer']) {
                        if (typeof onUpdated === typeof (function(){})) onUpdated();
                    } else {
                        if (typeof onActual === typeof (function(){})) onActual();
                    }
                },
                error: onError,
                complete: onComplete
            });
        } else {
            if (typeof onNoTimestamp === typeof (function(){})) onNoTimestamp();
        }
    },

    initializeMvcLinks: function () {
        $$(document).on('click', 'a[data-controller]', function(e) {
            var $a = $$(e.target);
            while (!$a.is('a[href]')) {
                $a = $a.parent();
            }

            var controllerName = $a.attr('data-controller');
            controllerName = controllerName.substr(0, 1).toUpperCase() + controllerName.substr(1) + 'Controller';
            console.log('MVC Call: ' + controllerName);
            var query = $a.attr('data-query');
            if (query) {
                console.log('MVC Query: ' + query);
                try {
                    query = JSON.parse(query);
                    console.log('MVC Parsed JSON-Query:');
                    console.log(query);
                } catch (e) {
                    console.log('Unparsable JSON');
                }
            }
            var ignoreCache = $a.is('[data-ignore-cache]');
            if (controllerName in window) {
                e.preventDefault();
                window[controllerName]().load(query, ignoreCache);
            }
        });
    },

    initializeMainView: function() {
        var thisPtr = this;

        // Add view
        mainView = myApp.addView('.view-main', {
            // Because we want to use dynamic navbar, we need to enable it for this view:
            // (Material doesn't support it but don't worry about it,
            // F7 will ignore it for Material theme)
            dynamicNavbar: true,
            domCache: true //чтобы навигация работала без сбоев и с запоминанием scroll position в длинных списках
        });

        console.log(myApp);
        console.log(mainView);

        app.initializeMvcLinks();

        $$(document).on('pageBeforeInit', function(e) {
            var context = {
                language: language,
                style: style,
                god: isGod,
                packVer: localStorage['packVer'],
                lastVerifiedTimestamp: localStorage['lastVerifiedTimestamp'] / 1000
            };

            var page = e.detail.page;
            //console.log(page);
            var $pageContainer = $$(page.container);
            var $tpls = $pageContainer.find('script[type="text/template"]');
            $tpls.each(function () {
                var $tpl = $$(this);
                var pageTpl = Handlebars.compile($tpl.html());
                var $compiledTpl = $$(pageTpl(context));
                $compiledTpl.insertBefore($tpl);
                $tpl.remove();
            });
        });

        if (isIos) {
            $$(document).on('navbarBeforeInit', function(e) {
                var context = {
                    language: language,
                    style: style,
                    god: isGod
                };

                var $navbar = $$(e.detail.navbar.innerContainer);
                var tpl = Handlebars.compile($navbar.html());
                $navbar.html(tpl(context));
            });
        }

        $$(document).on('pageInit', function (e) {
            $$(document).off('pageInit');
            app.onMainViewReady.call(thisPtr);
        });

        var unreadNewsCount = 0;
        var newsLastSeenTimestamp = parseInt(localStorage.getItem('newsLastSeenTimestamp'));
        for (var key in window.news) {
            if (window.news[key].published_at > newsLastSeenTimestamp) unreadNewsCount++;
        }
        app.compileTpl(cordova.file.applicationDirectory + 'www/templates/menu.hbs', 'left-menu', function() {
            var compiledMenuHtml = window.compiledTpls['left-menu']({
                menu: window.menu,
                language: language,
                unreadNewsCount: unreadNewsCount
            });
            $$('#left-menu-tpl').html(compiledMenuHtml);

            myApp.openPanel('left', false);

            ProgrammeController().load({
                tab: 0
            });
        });
    },

    onMainViewReady: function() {
        $$(document).on('pageInit', function (e) {
            // Get page data from event data
            var page = e.detail.page;

            if (isAndroid) {
                app.cloneBarsToPage($$(page.container));
            }
        });

        myApp.closeModal('.login-screen');

        setInterval(function () {
            var showNotification = function() {
                lockedBackgroundVerification = true;
                myApp.addNotification({
                    title: _('schedule updated title'),
                    message: _('schedule updated'),
                    //hold: 15000,
                    button: {
                        text: _('close')
                    },
                    onClick: function () {
                        window.plugins.spinnerDialog.show(_('please wait'), _('please wait'), true);
                        app.restart();
                    },
                    onClose: function () {
                        setTimeout(function () {
                            lockedBackgroundVerification = false;
                        }, 2000);
                    }
                });
            };

            if (!lockedBackgroundVerification) {
                //var interval = 10 * 60 * 1000;
                var interval = 30 * 1000;
                if (isGod) {
                    //interval = 5 * 1000;
                }
                if ((new Date()).getTime() > parseInt(localStorage['lastVerifiedTimestamp']) + interval) {
                    app.verifyScheduleTimestamp(
                        function () {}, // onNoTimestamp
                        function () {}, // onBeginVerifying
                        function () {}, // onSuccessVerifying
                        showNotification // onUpdated
                        // onActual
                        // onError
                        // onComplete
                    );
                }
            }
        }, 1000);
    },

    $getNavbar: function($page) {
        if (isAndroid) {
            return $$('#navbar');
        } else {
            var $transitioningNavbar = $$('#navbar').find('.navbar-from-left-to-center');
            if ($transitioningNavbar.length) {
                return $transitioningNavbar;
            } else {
                return $$('#navbar').find('.navbar-inner:last-child');
            }
        }
    },

    cloneBarsToPage: function($page) {
        /*var $navbar = $$($$('#navbar')[0].cloneNode(true)).removeAttr('id');
        var $toolbar = $$($$('#toolbar')[0].cloneNode(true)).removeAttr('id');

        $page.prepend($navbar);
        $page.append($toolbar);

        $navbar.find('.center').html($page.attr('data-title'));

        var $subnavbar = $page.find('.subnavbar');
        if ($subnavbar.length) {
            $navbar.append($subnavbar);
        }*/
    },

    resetNavBar: function () {
        app.$getNavbar().find('.right').html('');
        //app.$getNavbar().find('.left').find('.link.back').remove();
        myApp.sizeNavbars('.view-main');
    },

    setTitle: function (titleHTML) {
        app.$getNavbar().find('.center').html(titleHTML);
    },

    restart: function () {
        window.location.hash = '';
        window.location.reload();
    }
};

app.initialize();