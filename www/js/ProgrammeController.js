; // intentionally left here
(function() {
    var ProgrammeController = Object.create(BaseController);
    Object.defineProperty(ProgrammeController, 'treeByTabs', {enumerable: true, writable: true});
    Object.defineProperty(ProgrammeController, '_pageName', {value: 'programme/sessions'});
    Object.defineProperty(ProgrammeController, '_pageTitle', {value: 'Программа'});
    Object.defineProperty(ProgrammeController, '_filteredColourID', {value: null, writable: true});
    Object.defineProperty(ProgrammeController, '_btnFilterID', {value: null, writable: true});

    Object.defineProperty(ProgrammeController, 'onInit', {value: function ($pageContainer, page) {
        $pageContainer.find('.subnavbar .buttons-row a').each((function (idx, elem) {
            $$(elem).attr('href', '#tab' + idx + '-' + this._pageID);
        }).bind(this));
        $pageContainer.find('.tab').each((function (idx, elem) {
            $$(elem).attr('id', 'tab' + idx + '-' + this._pageID);
        }).bind(this));

        if (!('tab' in page.query)) {
            page.query.tab = 0;
        }

        myApp.showTab('#tab' + page.query.tab + '-' + this._pageID);

        this.onTabShow(page.query.tab);

        this.on('show', '.tab', (function (e) {
            var $tab = $$(e.target);
            var tabIdx = $tab.index();
            console.log($tab);
            console.log(tabIdx);
            this.onTabShow(tabIdx);
        }).bind(this));

        this.on('click', '.tab .card[data-role][data-no-caption]', (function (e) {
            console.log('this:');
            console.log(this);
            console.log(e);

            var $card = $$(e.target);
            while (!$card.is('.card')) {
                $card = $card.parent();
            }

            console.log('Card clicked:');
            console.log($card);

            this.onCardClick($card);
        }).bind(this));
    }});
    Object.defineProperty(ProgrammeController, 'onAfterAnimation', {value: function (page) {
        //this._filteredColourID = null;
        var timestamp = (new Date()).getTime();
        this._btnFilterID = 'btnFilter-' + timestamp;

        var $navbarRight = app.$getNavbar($$('#' + this._pageID)).find('.right');
        $navbarRight.html('<a href="#" class="link icon-only btnFilter" id="' + this._btnFilterID + '"><i class="custom-icons icon-filter-outline"></i></a>');
        this.onFilterChosen(this._filteredColourID);
        myApp.sizeNavbars('.view-main');

        $navbarRight.find('.btnFilter').on('click', (function (e) {
            this.onFilterClick();
        }).bind(this));
    }});

    Object.defineProperty(ProgrammeController, 'onFilterClick', {value: function () {
        var colours = Schedule.colours;
        var buttons = [];
        for (var colourID in colours) {
            colourID = parseInt(colourID);

            var colour = colours[colourID];

            var text = '<i class="f7-icons" style="color: ' + colour.circleCssColor + ';">';

            if (this._filteredColourID === colourID) {
                text += 'check_round_fill';
            } else {
                text += 'circle_fill';
            }
            text += '</i>';

            text += ' <span>' + colour['title_' + language] + '</span>';

            var button = {
                text: text,
                onClick: (function (colourID) {
                    this.onFilterChosen(colourID);
                }).bind(this, colourID)
            };
            buttons.push(button);
        }

        var noFilterText = '<i class="f7-icons">';
        if (this._filteredColourID === null) {
            noFilterText += 'check_round';
        } else {
            noFilterText += 'circle';
        }
        noFilterText += '</i>';

        var noFilter = {
            text: noFilterText + ' <span>' + _('show all') + '</span>',
            onClick: (function (colourID) {
                this.onFilterChosen(colourID);
            }).bind(this, null)
        };
        var groups = [buttons, [noFilter]];

        myApp.actions('#' + this._btnFilterID, groups);
    }});

    Object.defineProperty(ProgrammeController, 'onFilterChosen', {value: function (colourID) {
        this._filteredColourID = colourID;

        if (colourID !== null) {
            $$('.btnFilter').find('i.custom-icons').removeClass('icon-filter-outline').addClass('icon-filter-tool-black-shape');
            $$('#' + this._pageID).find('[data-role="session"]').hide();
            $$('#' + this._pageID).find('[data-role="session"][data-colour-id="' + colourID + '"]').show();
            $$('#' + this._pageID).find('[data-role="session"]:not([data-colour-id])').show();
        } else {
            $$('.btnFilter').find('i.custom-icons').addClass('icon-filter-outline').removeClass('icon-filter-tool-black-shape');
            $$('#' + this._pageID).find('[data-role="session"]').show();
        }
    }});

    Object.defineProperty(ProgrammeController, 'onTabShow', {
        value: (function (tabIdx) {
            console.log('Schedule for tab:');
            console.log(this.treeByTabs['tab' + (tabIdx + 1)]);

            var $tab = $$('#tab' + tabIdx + '-' + this._pageID);
            if (!$tab.attr('data-cached')) {
                setTimeout((function () {
                    var $content = $tab.find('.content-block').html('');
                    var $preloader = $$('<span />');
                    $preloader.addClass('preloader').css('width', '50px').css('height', '50px').html($$('#tpl-preloader').html());
                    $preloader.appendTo($content);

                    setTimeout((function () {
                        var $collection = this._buildDOMForTab(tabIdx);
                        $collection.appendTo($content);
                        $preloader.remove();
                        $tab.attr('data-cached', 'true');
                        this.onFilterChosen(this._filteredColourID);
                    }).bind(this), 100);
                }).bind(this), 0);
            }
        }).bind(ProgrammeController)
    });
    Object.defineProperty(ProgrammeController, 'onCardClick', {
        value: (function ($card) {
            SessionController().load({
                sessionID: parseInt($card.attr('data-session-id'))
            })
        }).bind(ProgrammeController)
    });

    Object.defineProperty(ProgrammeController, '_buildDOMForTab', {value: function(tabIdx) {
        try {
            //window.plugins.spinnerDialog.show(_('please wait'), _('building dom'), true);

            var $collection = $$();
            var $sessionsList = null;
            var $sessionsUl = null;
            var $sessionCard;
            var chairmenArr, narratorsArr;

            this.treeByTabs['tab' + (tabIdx + 1)].forEach((function (session) {
                if (!session['title_' + language]) {
                    return;
                }

                if (!$sessionsList) {
                    $sessionsList = $$('<div />');
                    $sessionsList.addClass('list-block cards-list');
                }
                if (!$sessionsUl) {
                    $sessionsUl = $$('<ul />');
                }

                var context = {
                    language: language,
                    session: session
                };

                var compiledHtml = window.compiledTpls.sessionCard(context).trim();
                $sessionCard = $$(compiledHtml);

                if (session.colour) {
                    if (this._filteredColourID !== null) {
                        if (this._filteredColourID !== session.colour.id) {
                            $sessionCard.hide();
                        }
                    }
                }

                $sessionCard.appendTo($sessionsUl);
            }).bind(this));

            if ($sessionsList && $sessionsUl) {
                $sessionsUl.appendTo($sessionsList);
                $collection.add($sessionsList);
            }

            $collection.add($$('<p style="text-align: center;">Версия расписания: ' + localStorage['packVer'] + '</p>'));

            //window.plugins.spinnerDialog.hide();
            return $collection;
        } catch (e) {
            console.error(e);
            console.groupCollapsed();
            console.log(e.stack);
            console.groupEnd();
            myApp.alert('Exception');
        }
    }});

    window.ProgrammeController = function () {
        return ProgrammeController._construct();
    };
})();
