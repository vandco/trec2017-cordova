; // intentionally left here
(function() {
    var SpeakerController = Object.create(BaseController);
    Object.defineProperty(SpeakerController, '_pageName', {value: 'programme/speaker'});
    Object.defineProperty(SpeakerController, '_pageTitle', {value: 'Спикер'});
    Object.defineProperty(SpeakerController, '_speaker', {writable: true});
    Object.defineProperty(SpeakerController, '_speakerRoles_cached', {writable: true, value: {}});
    Object.defineProperty(SpeakerController, '_mode', {writable: true, value: 'byChronology'});
    Object.defineProperty(SpeakerController, 'onInit', {value: function ($pageContainer, page) {
        this._speaker = Schedule.speakers[page.query.speakerID];

        var $content = $pageContainer.find('.content-block').html('');
        var $preloader = $$('<span />');
        $preloader.addClass('preloader').css('width', '50px').css('height', '50px').html($$('#tpl-preloader').html());
        $preloader.appendTo($content);

        setTimeout((function () {
            var $collection = this._buildDOM();
            $collection.appendTo($content);
            $preloader.remove();
        }).bind(this), 100);

        this.on('click', '.card', (function (e) {
            console.log('this:');
            console.log(this);
            console.log(e);

            var $card = $$(e.target);
            while (!$card.is('.card')) {
                $card = $card.parent();
            }

            console.log('Card clicked:');
            console.log($card);

            this.onCardClick($card);
        }).bind(this));

        this.on('click', '.btnByChronology', (function (e) {
            this._mode = 'byChronology';
            this._rebuildTimetableDOM();
        }).bind(this));

        this.on('click', '.btnByRoles', (function (e) {
            this._mode = 'byRoles';
            this._rebuildTimetableDOM();
        }).bind(this));
    }});
    Object.defineProperty(SpeakerController, 'onAfterAnimation', {value: function (page) {
        var $navbarRight = app.$getNavbar($$('#' + this._pageID)).find('.right');
        var $btnBack = $$('<a href="#" class="link back" />');
        $btnBack.html(_('back')).appendTo($navbarRight);
        myApp.sizeNavbars('.view-main');

        this.onBackButton = function() {
            $btnBack.click();
            return false;
        };
    }});
    Object.defineProperty(SpeakerController, 'onCardClick', {
        value: (function ($card) {
            switch ($card.attr('data-role')) {
                case 'session':
                    SessionController().load({
                        sessionID: parseInt($card.attr('data-session-id'))
                    });
                    break;
                case 'lecture':
                    LectureController().load({
                        lectureID: parseInt($card.attr('data-lecture-id'))
                    });
                    break;
                default:
                    myApp.alert('Непонятная карточка!');
                    break;
            }
        }).bind(SpeakerController)
    });

    Object.defineProperty(SpeakerController, '_buildDOM', {value: function() {
        try {
            var $collection = $$();
            var $list = null;
            var $ul = null;
            var $card, $sessionCard, $title, $sessionTitle, $sessionContent, $sessionContentInner, chairmenArr, narratorsArr, $p, $sessionFooter, $sessionFooterCircumstances, $hall, $cardContent, $cardContentInner, $cardFooter, $roles, $cardHeader, $time, $cardFooterCircumstances, $details;
            var roles;

            // Speaker Photo
            var photoSrc = '';
            if (typeof this._speaker.photoName !== typeof undefined) {
                photoSrc = cordova.file.cacheDirectory + 'unpacked/' + this._speaker.photoName;
            } else {
                if (this._speaker.sex === 'female') {
                    photoSrc = 'img/female_100.png';
                } else {
                    photoSrc = 'img/male_100.png';
                }
            }

            var $img = $$('<img />');
            $img
                .attr('src', photoSrc)
                .css({
                    'margin': '10px auto',
                    'display': 'block',
                    'border-radius': '50%',
                    'max-width': '100px'
                });
            $collection.add($img);


            // Speaker Name
            var $h1 = $$('<h1 />');
            $h1.html(this._speaker['name_' + language]);
            $collection.add($h1);



            // Mode Switcher
            var $switcherHeader = $$('<div class="content-block-title" />');
            $switcherHeader.html(_('individual timetable'));
            $collection.add($switcherHeader);
            var $switcher = $$('<p />');
            $switcher.addClass('buttons-row');
            $$('<a href="#" class="button button-raised button-fill btnByChronology">' + _('by chronology') + '</a>').appendTo($switcher);
            $$('<a href="#" class="button button-raised btnByRoles">' + _('by roles') + '</a>').appendTo($switcher);
            $collection.add($switcher);



            // Timetable
            var $timetable = $$('<div class="timetable" />');
            $collection.add($timetable);


            setTimeout((function () {
                this._rebuildTimetableDOM();
            }).bind(this), 0);


            return $collection;
        } catch (e) {
            console.error(e);
            console.groupCollapsed();
            console.log(e.stack);
            console.groupEnd();
            myApp.alert('Exception');
        }
    }});

    Object.defineProperty(SpeakerController, '_rebuildTimetableDOM', {value: function () {
        var $buttons = $$('#' + this._pageID).find('.buttons-row').find('.button');
        $buttons.removeClass('button-fill');
        var $activeButton;
        if (this._mode == 'byRoles') {
            $activeButton = $$('#' + this._pageID).find('.btnByRoles');
        } else {
            $activeButton = $$('#' + this._pageID).find('.btnByChronology');
        }
        $activeButton.addClass('button-fill');

        var $content = $$('#' + this._pageID).find('.timetable').html('');
        var $preloader = $$('<span />');
        $preloader.addClass('preloader').css('width', '50px').css('height', '50px').html($$('#tpl-preloader').html());
        $preloader.appendTo($content);

        setTimeout((function () {
            var $collection;
            if (this._mode == 'byRoles') {
                $collection = this._buildDOMByRoles();
            } else {
                $collection = this._buildDOMByChronology();
            }
            $collection.appendTo($content);
            $preloader.remove();
        }).bind(this), 100);
    }});

    Object.defineProperty(SpeakerController, '_buildDOMByRoles', {value: function() {
        try {
            var $collection = $$();
            var $list = null;
            var $ul = null;
            var $card, $sessionCard, $title;

            // As Chairman

            var sessionsAsChairmanCount = 0;
            this._speaker.sessionsAsChairman.forEach(function (session) {
                if (!session['title_' + language]) {
                    return;
                } else {
                    sessionsAsChairmanCount++;
                }
            });
            if (sessionsAsChairmanCount) {
                $title = $$('<div />');
                $title.addClass('content-block-title').html(_('chairman'));
                $collection.add($title);

                $list = $$('<div />');
                $list.addClass('list-block cards-list');
                $ul = $$('<ul />');
                this._speaker.sessionsAsChairman.forEach((function (session) {
                    if (!session['title_' + language]) {
                        return;
                    }

                    var context = {
                        language: language,
                        session: session,
                        date: true,
                        activeSpeaker: this._speaker
                    };
                    var compiledHtml = window.compiledTpls.sessionCard(context).trim();
                    $sessionCard = $$(compiledHtml);

                    $sessionCard.appendTo($ul);
                }).bind(this));
                $ul.appendTo($list);
                $collection.add($list);
            }


            // As Narrator

            var sessionsAsNarratorCount = 0;
            this._speaker.sessionsAsNarrator.forEach(function (session) {
                if (!session['title_' + language]) {
                    return;
                } else {
                    sessionsAsNarratorCount++;
                }
            });
            if (sessionsAsNarratorCount) {
                $title = $$('<div />');
                $title.addClass('content-block-title').html(_('narrator'));
                $collection.add($title);

                $list = $$('<div />');
                $list.addClass('list-block cards-list');
                $ul = $$('<ul />');
                this._speaker.sessionsAsNarrator.forEach((function (session) {
                    if (!session['title_' + language]) {
                        return;
                    }

                    var context = {
                        language: language,
                        session: session,
                        date: true,
                        activeSpeaker: this._speaker
                    };
                    var compiledHtml = window.compiledTpls.sessionCard(context).trim();
                    $sessionCard = $$(compiledHtml);

                    $sessionCard.appendTo($ul);
                }).bind(this));
                $ul.appendTo($list);
                $collection.add($list);
            }


            // As Speaker

            var lecturesCount = 0;
            this._speaker.lectures.forEach(function (lecture) {
                if (lecture.sessions.length != 1) {
                    return;
                } else {
                    lecturesCount++;
                }
            });

            if (lecturesCount) {
                $title = $$('<div />');
                $title.addClass('content-block-title').html(_('speaker'));
                $collection.add($title);

                $list = $$('<div />');
                $list.addClass('list-block cards-list');
                $ul = $$('<ul />');
                this._speaker.lectures.forEach((function (lecture) {
                    if (lecture.sessions.length != 1) {
                        return;
                    }

                    var context = {
                        language: language,
                        lecture: lecture,
                        date: true,
                        activeSpeaker: this._speaker
                    };
                    var compiledHtml = window.compiledTpls.lectureCard(context).trim();
                    $card = $$(compiledHtml);

                    $card.appendTo($ul);
                }).bind(this));
                $ul.appendTo($list);
                $collection.add($list);
            }


            return $collection;
        } catch (e) {
            console.error(e);
            console.groupCollapsed();
            console.log(e.stack);
            console.groupEnd();
            myApp.alert('Exception');
        }
    }});

    Object.defineProperty(SpeakerController, '_buildDOMByChronology', {value: function() {
        try {
            var $collection = $$();
            var $list = null;
            var $ul = null;
            var $card, $sessionCard, $title;
            var context;

            var events = [];
            this._speaker.sessionsAsChairman.forEach(function (session) {
                events.push(session);
            });
            this._speaker.sessionsAsNarrator.forEach(function (session) {
                events.push(session);
            });
            this._speaker.lectures.forEach(function (lecture) {
                if (lecture.sessions.length != 1) {
                    return;
                }

                events.push(lecture);
            });

            var fieldName = 'title_' + language;
            events.sort(
                firstBy(function (a, b) {
                    if (a.start.getTime() > b.start.getTime()) return 1;
                    if (a.start.getTime() == b.start.getTime()) return 0;
                    if (a.start.getTime() < b.start.getTime()) return -1;
                }).thenBy(function(a, b) {
                    if (a.end.getTime() > b.end.getTime()) return 1;
                    if (a.end.getTime() == b.end.getTime()) return 0;
                    if (a.end.getTime() < b.end.getTime()) return -1;
                }).thenBy(function(a, b) {
                    if (a[fieldName] >  b[fieldName]) return 1;
                    if (a[fieldName] == b[fieldName]) return 0;
                    if (a[fieldName] <  b[fieldName]) return -1;
                })
            );

            $list = $$('<div />');
            $list.addClass('list-block cards-list');
            $ul = $$('<ul />');
            events.forEach((function (event) {
                if (event.toString() === '[object Session]') {
                    context = {
                        language: language,
                        session: event,
                        date: true,
                        activeSpeaker: this._speaker
                    };
                    var compiledHtml = window.compiledTpls.sessionCard(context).trim();
                    $sessionCard = $$(compiledHtml);

                    $sessionCard.appendTo($ul);
                } else if (event.toString() === '[object Lecture]') {
                    context = {
                        language: language,
                        lecture: event,
                        date: true,
                        activeSpeaker: this._speaker
                    };
                    compiledHtml = window.compiledTpls.lectureCard(context).trim();
                    $card = $$(compiledHtml);

                    $card.appendTo($ul);
                }
            }).bind(this));
            $ul.appendTo($list);
            $collection.add($list);


            return $collection;
        } catch (e) {
            console.error(e);
            console.groupCollapsed();
            console.log(e.stack);
            console.groupEnd();
            myApp.alert('Exception');
        }
    }});

    window.SpeakerController = function () {
        return SpeakerController._construct();
    };
})();
