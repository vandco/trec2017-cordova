; // intentionally left here
(function() {
    var VotingController = Object.create(BaseController);
    Object.defineProperty(VotingController, '_pageName', {value: 'voting'});
    Object.defineProperty(VotingController, '_pageTitle', {value: 'Голосование'});
    Object.defineProperty(VotingController, '_$pageContainer', {writable: true});
    Object.defineProperty(VotingController, '_reactiveData', {writable: true, value: {
        poll: {},
        pollTimestamp: null,
        netError: null,
        myVotes: {},
        selectedOptionID: null,
        isSubmitting: false,
    }});
    Object.defineProperty(VotingController, '_vueApp', {writable: true});
    Object.defineProperty(VotingController, 'onInit', {value: function ($pageContainer, page) {
        var $pageContent = $pageContainer.find('.content-block');
        this._$pageContainer = $pageContainer;

        this._reactiveData.myVotes = JSON.parse(localStorage.getItem('myVotes'));
        if (this._reactiveData.myVotes === null) {
            this._reactiveData.myVotes = {};
            localStorage.setItem('myVotes', JSON.stringify(this._reactiveData.myVotes));
        }

        this._vueApp = window.initVotingApp(this._reactiveData);

        var loadAndProcessPoll = (function() {
            this.loadPoll((function(poll, pollTimestamp) {
                this._reactiveData.poll = poll;
                this._reactiveData.pollTimestamp = pollTimestamp;
            }).bind(this));
        }).bind(this);

        loadAndProcessPoll();

        this.on('click', '.btnRetry', (function (e) {
            loadAndProcessPoll();
        }).bind(this));

        this.on('click', '[data-option-id]', (function (e) {
            var $option = $$(e.target);
            while (!$option.is('[data-option-id]')) {
                $option = $option.parent();
            }

            console.log('Option clicked:');
            console.log($option);

            this.onOptionClick($option);
        }).bind(this));

        this.on('click', '.btnVote', (function (e) {
            var $btnVote = $$(e.target);
            while (!$btnVote.is('.btnVote')) {
                $btnVote = $btnVote.parent();
            }

            this.onVoteClick();
        }).bind(this));
    }});
    Object.defineProperty(VotingController, 'onAfterAnimation', {value: function (page) {
        var $navbarRight = app.$getNavbar($$('#' + this._pageID)).find('.right');
        myApp.sizeNavbars('.view-main');
    }});
    Object.defineProperty(VotingController, 'onUnload', {value: function () {
        this._vueApp.$destroy();
        console.log('Vue.JS app destroyed');
        console.log(this._reactiveData);
        return true; // to unload the controller completely
    }});

    Object.defineProperty(VotingController, 'loadPoll', {
        value: (function (callback) {
            this._reactiveData.netError = null;
            this._reactiveData.selectedOptionID = null;

            $$.ajax({
                url: 'https://trec-course.ru/mobile-app/currentPoll',
                //crossDomain: true,
                dataType: 'json',
                timeout: 5000,
                method: 'GET',
                success: (function(data) {
                    console.log(data);
                    callback(data.poll, data.timestamp);

                    this._reactiveData.netError = false;
                }).bind(this),
                error: (function (xhr, status) {
                    console.log(xhr);
                    console.log(status);

                    this._reactiveData.netError = true;
                }).bind(this)
            });
        }).bind(VotingController)
    });

    Object.defineProperty(VotingController, 'updatePoll', {
        value: (function (poll) {
            this._reactiveData.poll = poll;
        }).bind(VotingController)
    });

    Object.defineProperty(VotingController, 'onOptionClick', {
        value: (function ($option) {
            var $pageContent = this._$pageContainer.find('.page-content').find('.content-block');
            //$pageContent.find('[data-option-id]').removeAttr('data-checked').find('.item-media .f7-icons').html('circle');
            //$option.attr('data-checked', true).find('.item-media .f7-icons').html('check_round');
            this._reactiveData.selectedOptionID = parseInt($option.attr('data-option-id'));

            //$pageContent.find('.btnVote').removeClass('disabled');
        }).bind(VotingController)
    });

    Object.defineProperty(VotingController, 'onVoteClick', {
        value: (function () {
            var $pageContent = this._$pageContainer.find('.page-content').find('.content-block');

            //$pageContent.find('.btnVote').remove();

            /*var options = $pageContent.find('[data-option-id]'); // it's some weird hell, not an array
            var selectedOptionID;
            for (var idx = 0; idx < options.length; idx++) {
                var $option = $$(options[idx]);
                var $icon = $option.find('.item-media .f7-icons');
                if (!$option.attr('data-checked')) {
                    $icon.css('visibility', 'hidden');
                } else {
                    $icon.html('check_round_fill');
                    selectedOptionID = parseInt($option.attr('data-option-id'));
                }
            }*/

            this._reactiveData.isSubmitting = true;

            this.submitVote(this._reactiveData.poll.id, this._reactiveData.selectedOptionID, (function(poll, pollTimestamp) {
                this._reactiveData.isSubmitting = false;

                this._reactiveData.poll = poll;
                this._reactiveData.pollTimestamp = pollTimestamp;

                // save submitted vote
                this._reactiveData.myVotes[this._reactiveData.poll.id] = this._reactiveData.selectedOptionID;
                localStorage.setItem('myVotes', JSON.stringify(this._reactiveData.myVotes));
            }).bind(this));
        }).bind(VotingController)
    });

    Object.defineProperty(VotingController, 'submitVote', {
        value: (function (pollID, optionID, callback) {
            $$.ajax({
                url: 'https://trec-course.ru/mobile-app/vote/' + pollID,
                //crossDomain: true,
                dataType: 'json',
                timeout: 5000,
                method: 'POST',
                data: {
                    optionID: optionID,
                    uniqueID: Math.random().toString() + (new Date()).getTime().toString()
                },
                success: function(data) {
                    console.log(data);
                    callback(data.poll, data.timestamp);
                },
                /*error: function (xhr, status) {
                    console.log(xhr);
                    console.log(status);
                    if (status === 'timeout') {
                        callbackOnConnectivityFailure();
                    } else {
                        callbackOnError(xhr.responseText);
                    }
                }*/
            });
        }).bind(VotingController)
    });



    window.VotingController = function () {
        return VotingController._construct();
    };
})();
