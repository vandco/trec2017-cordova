; // intentionally left here
(function() {
    var SessionController = Object.create(BaseController);
    Object.defineProperty(SessionController, '_pageName', {value: 'programme/session'});
    Object.defineProperty(SessionController, '_pageTitle', {value: 'Сессия'});
    Object.defineProperty(SessionController, '_session', {writable: true});
    Object.defineProperty(SessionController, '_speakerRoles_cached', {writable: true, value: {}});
    Object.defineProperty(SessionController, 'onInit', {value: function ($pageContainer, page) {
        this._session = Schedule.sessions[page.query.sessionID];

        var $content = $pageContainer.find('.content-block').html('');
        var $preloader = $$('<span />');
        $preloader.addClass('preloader').css('width', '50px').css('height', '50px').html($$('#tpl-preloader').html());
        $preloader.appendTo($content);

        setTimeout((function () {
            var $collection = this._buildDOM();
            $collection.appendTo($content);
            $preloader.remove();
        }).bind(this), 1500);

        this.on('click', '.card[data-role]', (function (e) {
            console.log('this:');
            console.log(this);
            console.log(e);

            var $card = $$(e.target);
            while (!$card.is('.card')) {
                $card = $card.parent();
            }

            console.log('Card clicked:');
            console.log($card);

            this.onCardClick($card);
        }).bind(this));

        this.on('click', '.btnAddEvent', (function (e) {
            e.preventDefault();

            var $pageContent = $$('#' + this._pageID).find('.content-block');
            var $btnAddEvent = $pageContent.find('.btnAddEvent');
            var $btnRemoveEvent = $pageContent.find('.btnRemoveEvent');

            this.addEvent($btnAddEvent, $btnRemoveEvent);
        }).bind(this));

        this.on('click', '.btnRemoveEvent', (function (e) {
            e.preventDefault();

            var $pageContent = $$('#' + this._pageID).find('.content-block');
            var $btnAddEvent = $pageContent.find('.btnAddEvent');
            var $btnRemoveEvent = $pageContent.find('.btnRemoveEvent');

            this.removeEvent($btnAddEvent, $btnRemoveEvent);
        }).bind(this));
    }});
    Object.defineProperty(SessionController, 'onAfterAnimation', {value: function (page) {
        var $navbarRight = app.$getNavbar($$('#' + this._pageID)).find('.right');
        var $btnBack = $$('<a href="#" class="link back" />');
        $btnBack.html(_('back')).appendTo($navbarRight);
        myApp.sizeNavbars('.view-main');

        this.onBackButton = function() {
            $btnBack.click();
            return false;
        };
    }});
    Object.defineProperty(SessionController, 'onCardClick', {
        value: (function ($card) {
            switch ($card.attr('data-role')) {
                case 'speaker':
                    SpeakerController().load({
                        speakerID: parseInt($card.attr('data-speaker-id'))
                    });
                    break;
                case 'lecture':
                    LectureController().load({
                        lectureID: parseInt($card.attr('data-lecture-id'))
                    });
                    break;
                default:
                    myApp.alert('Непонятная карточка!');
                    break;
            }
        }).bind(SessionController)
    });

    Object.defineProperty(SessionController, '_buildDOMForSpeakerCard', {value: function($card, speaker, rolesStr) {
        $card.addClass('card speaker').attr('data-role', 'speaker').attr('data-speaker-id', speaker.id);

        var $cardContent = $$('<div />');
        $cardContent.addClass('card-content');

        var $cardContentInner = $$('<div />');
        $cardContentInner.addClass('card-content-inner');

        var photoSrc = '';
        if (typeof speaker.photoName !== typeof undefined) {
            photoSrc = cordova.file.cacheDirectory + 'unpacked/' + speaker.photoName;
        } else {
            if (speaker.sex === 'female') {
                photoSrc = 'img/female_100.png';
            } else {
                photoSrc = 'img/male_100.png';
            }
        }

        var $photo = $$('<img />');
        $photo
            .attr('src', photoSrc)
            .css('margin-right', '20px')
            .appendTo($cardContentInner);

        var $span = $$('<span />');
        $span.html(speaker['name_' + language]).appendTo($cardContentInner);

        $cardContentInner.appendTo($cardContent);
        $cardContent.appendTo($card);

        var $cardFooter = $$('<div />');
        $cardFooter.addClass('card-footer');
        var $roles = $$('<span />');
        $roles.appendTo($cardFooter);
        var $details = $$('<div />');
        $details.html(_('in details')).addClass('details').appendTo($cardFooter);
        $cardFooter.appendTo($card);

        return $card;
    }});

    Object.defineProperty(SessionController, '_buildDOM', {value: function() {
        //window.plugins.spinnerDialog.show(_('please wait'), _('building dom'), true);

        var $collection = $$();
        var $list = null;
        var $ul = null;
        var $card, $title, $cardContent, $cardContentInner, $cardFooter, $roles, $cardHeader, $time, $details, $p;

        // Session Title
        var $h1 = $$('<h1 />');
        $h1.html(this._session['title_' + language]);
        $collection.add($h1);

        // Session Time
        var $h2 = $$('<h2 />');
        $h2.html(
            _("date[format]", {
                day: this._session.start.getDate().toString(),
                month: _("date[month][" + this._session.start.getMonth() + "][full]")
            })
            + ', '
            + this._session.start.getHours() + ':' + this._session.start.getMinutes().toString().paddingLeft("00")
            + ' – '
            + this._session.end.getHours() + ':' + this._session.end.getMinutes().toString().paddingLeft("00")
        );
        $collection.add($h2);

        // Hall

        if (this._session.hall) {
            var $h3 = $$('<h3 />');
            $h3.html(this._session.hall['title_' + language]);
            $collection.add($h3);
        }


        // My Agenda
        var $myAgendaAdd = $$('<a />');
        $myAgendaAdd.addClass('button button-raised').attr('href', '#');
        $myAgendaAdd.html(_('add to my agenda')).addClass('btnAddEvent');
        $collection.add($myAgendaAdd);

        var $myAgendaRemove = $$('<a />');
        $myAgendaRemove.addClass('button button-raised').attr('href', '#');
        $myAgendaRemove.html(_('remove from my agenda')).addClass('btnRemoveEvent');
        $collection.add($myAgendaRemove);

        if (MyAgenda.isIn(this._session)) {
            $myAgendaAdd.hide();
        } else {
            $myAgendaRemove.hide();
        }


        // Chairmen

        if (this._session.chairmen.length) {
            $title = $$('<div />');
            $title.addClass('content-block-title').html(_('chairmen'));
            $collection.add($title);

            $list = $$('<div />');
            $list.addClass('list-block cards-list');
            $ul = $$('<ul />');
            this._session.chairmen.forEach((function (speaker) {
                $card = $$('<li />');

                this._buildDOMForSpeakerCard($card, speaker).appendTo($ul);
            }).bind(this));
            $ul.appendTo($list);
            $collection.add($list);
        }

        // Narrators

        if (this._session.narrators.length) {
            $title = $$('<div />');
            $title.addClass('content-block-title').html(_('narrators'));
            $collection.add($title);

            $list = $$('<div />');
            $list.addClass('list-block cards-list');
            $ul = $$('<ul />');
            this._session.narrators.forEach((function (speaker) {
                $card = $$('<li />');

                this._buildDOMForSpeakerCard($card, speaker).appendTo($ul);
            }).bind(this));
            $ul.appendTo($list);
            $collection.add($list);
        }


        // Lectures

        if (this._session.lectures.length) {
            $title = $$('<div />');
            $title.addClass('content-block-title').html(_('lectures'));
            $collection.add($title);

            $list = $$('<div />');
            $list.addClass('list-block cards-list');
            $ul = $$('<ul />');
            this._session.lectures.forEach((function (lectureObj) {
                $card = $$('<li />');
                $card.addClass('card').attr('data-lecture-id', lectureObj.lecture.id);

                $cardHeader = $$('<div />');
                $cardHeader.addClass('card-header').html('<div>' + lectureObj.lecture['title_' + language] + '</div>').appendTo($card);

                if (this._session.colour) {
                    $card.css('background-color', this._session.colour.bgCssColor);
                }

                if (!lectureObj.lecture.is_caption) {
                    $card.addClass('card').attr('data-role', 'lecture');

                    $cardContent = $$('<div />');
                    $cardContent.addClass('card-content');
                    $cardContentInner = $$('<div />');
                    $cardContentInner.addClass('card-content-inner');

                    if (lectureObj.lecture.speakers.length) {
                        var speakers = [];
                        lectureObj.lecture.speakers.forEach(function (speaker) {
                            speakers.push(speaker['name_' + language]);
                        });
                        $p = $$('<p />');
                        $p.html(speakers.join(', ')).appendTo($cardContentInner);
                    }

                    $p = $$('<p />');
                    $time = $$('<span />');
                    $time.addClass('time').html(
                        lectureObj.start.getHours() + ':' + lectureObj.start.getMinutes().toString().paddingLeft("00")
                        + ' – '
                        + lectureObj.end.getHours() + ':' + lectureObj.end.getMinutes().toString().paddingLeft("00")
                    ).appendTo($p);
                    $p.appendTo($cardContentInner);

                    $cardContentInner.appendTo($cardContent);
                    $cardContent.appendTo($card);

                    $cardFooter = $$('<div />');
                    $cardFooter.addClass('card-footer');

                    $details = $$('<div />');
                    $details.html(_('in details')).addClass('details');
                    $$('<p />').appendTo($cardFooter);
                    $details.appendTo($cardFooter);

                    $cardFooter.appendTo($card);
                }

                $card.appendTo($ul);
            }).bind(this));
            $ul.appendTo($list);
            $collection.add($list);
        }


        //window.plugins.spinnerDialog.hide();
        return $collection;
    }});

    Object.defineProperty(SessionController, 'addEvent', {value: function ($btnAddEvent, $btnRemoveEvent) {
        window.plugins.spinnerDialog.show(_('please wait'), _('adding event'), true);
        MyAgenda.addEvent(this._session);
        $btnAddEvent.hide();
        $btnRemoveEvent.show();
        window.plugins.spinnerDialog.hide();
    }});

    Object.defineProperty(SessionController, 'removeEvent', {value: function ($btnAddEvent, $btnRemoveEvent) {
        window.plugins.spinnerDialog.show(_('please wait'), _('removing event'), true);
        MyAgenda.removeEvent(this._session);
        $btnRemoveEvent.hide();
        $btnAddEvent.show();
        window.plugins.spinnerDialog.hide();
    }});


    window.SessionController = function () {
        return SessionController._construct();
    };
})();
