/**
 * Created by artur on 21/03/2017.
 * @see http://jsfiddle.net/n4ncq2nc/1/
 * @see http://stackoverflow.com/a/18045190
 * @author Teun D
 * @author shelbalart (modifications)
 */
; // intentionally left here
(function() {
    window.firstBy = (function() {
        function e(f) {
            f.thenBy = t;
            return f;
        }

        function t(y, x) {
            x = this;
            return e(function(a, b) {
                var prev = x(a, b);
                if (prev == 0) {
                    return y(a, b);
                } else {
                    return prev;
                }
                //return x(a, b) || y(a, b);
            });
        }

        return e;
    })();
})();
