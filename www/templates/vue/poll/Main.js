import Vue from 'vue';

import VotingApp from './VotingApp.vue';

window.initVotingApp = function(reactiveData) {
    const globals = new Vue({
        data: reactiveData,
    });
    globals.install = function () {
        Object.defineProperty(Vue.prototype, '$globals', {
            get() {
                return globals;
            },
            configurable: true,
        });
    };
    Vue.use(globals);

    return new Vue({
        el: '#voting-app',
        render: h => h(VotingApp),
    });
};
