#!/usr/bin/env node

/**
 * Created by USER on 23.03.2017.
 * @author Holly Schinsky
 * @see http://devgirl.org/2013/11/12/three-hooks-your-cordovaphonegap-project-needs/
 */

//
// This hook copies various resource files
// from our version control system directories
// into the appropriate platform specific location
//


// configure all the files to copy.
// Key of object is the source file,
// value is the destination location.
// It's fine to put all platforms' icons
// and splash screen files here, even if
// we don't build for all platforms
// on each developer's box.

var filesToCopy = [
    {"res/mipmap-hdpi/icon.png": "platforms/android/res/mipmap-hdpi/icon.png"},
    {"res/mipmap-ldpi/icon.png": "platforms/android/res/mipmap-ldpi/icon.png"},
    {"res/mipmap-mdpi/icon.png": "platforms/android/res/mipmap-mdpi/icon.png"},
    {"res/mipmap-xhdpi/icon.png": "platforms/android/res/mipmap-xhdpi/icon.png"},
    {"res/mipmap-xxhdpi/icon.png": "platforms/android/res/mipmap-xxhdpi/icon.png"},
    {"res/mipmap-xxxhdpi/icon.png": "platforms/android/res/mipmap-xxxhdpi/icon.png"},

    {"res/ios/icon/icon.png": "platforms/ios/ТРЭК 2018/Images.xcassets/AppIcon.appiconset/icon.png"},
    {"res/ios/icon/icon@2x.png": "platforms/ios/ТРЭК 2018/Images.xcassets/AppIcon.appiconset/icon@2x.png"},
    {"res/ios/icon/icon-40.png": "platforms/ios/ТРЭК 2018/Images.xcassets/AppIcon.appiconset/icon-40.png"},
    {"res/ios/icon/icon-40@2x.png": "platforms/ios/ТРЭК 2018/Images.xcassets/AppIcon.appiconset/icon-40@2x.png"},
    {"res/ios/icon/icon-50.png": "platforms/ios/ТРЭК 2018/Images.xcassets/AppIcon.appiconset/icon-50.png"},
    {"res/ios/icon/icon-50@2x.png": "platforms/ios/ТРЭК 2018/Images.xcassets/AppIcon.appiconset/icon-50@2x.png"},
    {"res/ios/icon/icon-60@2x.png": "platforms/ios/ТРЭК 2018/Images.xcassets/AppIcon.appiconset/icon-60@2x.png"},
    {"res/ios/icon/icon-60@3x.png": "platforms/ios/ТРЭК 2018/Images.xcassets/AppIcon.appiconset/icon-60@3x.png"},
    {"res/ios/icon/icon-72.png": "platforms/ios/ТРЭК 2018/Images.xcassets/AppIcon.appiconset/icon-72.png"},
    {"res/ios/icon/icon-72@2x.png": "platforms/ios/ТРЭК 2018/Images.xcassets/AppIcon.appiconset/icon-72@2x.png"},
    {"res/ios/icon/icon-76.png": "platforms/ios/ТРЭК 2018/Images.xcassets/AppIcon.appiconset/icon-76.png"},
    {"res/ios/icon/icon-76@2x.png": "platforms/ios/ТРЭК 2018/Images.xcassets/AppIcon.appiconset/icon-76@2x.png"},
    {"res/ios/icon/icon-83.5@2x.png": "platforms/ios/ТРЭК 2018/Images.xcassets/AppIcon.appiconset/icon-83.5@2x.png"},
    {"res/ios/icon/icon-small.png": "platforms/ios/ТРЭК 2018/Images.xcassets/AppIcon.appiconset/icon-small.png"},
    {"res/ios/icon/icon-small@2x.png": "platforms/ios/ТРЭК 2018/Images.xcassets/AppIcon.appiconset/icon-small@2x.png"},
    {"res/ios/icon/icon-small@3x.png": "platforms/ios/ТРЭК 2018/Images.xcassets/AppIcon.appiconset/icon-small@3x.png"},
    {"res/ios/icon/app-store-icon.png": "platforms/ios/ТРЭК 2018/Images.xcassets/AppIcon.appiconset/app-store-icon.png"},

    {"res/ios/splash/Default@2x~iphone.png": "platforms/ios/ТРЭК 2018/Images.xcassets/LaunchImage.launchimage/Default@2x~iphone.png"},
    {"res/ios/splash/Default-568h@2x~iphone.png": "platforms/ios/ТРЭК 2018/Images.xcassets/LaunchImage.launchimage/Default-568h@2x~iphone.png"},
    {"res/ios/splash/Default-667h.png": "platforms/ios/ТРЭК 2018/Images.xcassets/LaunchImage.launchimage/Default-667h.png"},
    {"res/ios/splash/Default-736h.png": "platforms/ios/ТРЭК 2018/Images.xcassets/LaunchImage.launchimage/Default-736h.png"},
    {"res/ios/splash/Default-Landscape@2x~ipad.png": "platforms/ios/ТРЭК 2018/Images.xcassets/LaunchImage.launchimage/Default-Landscape@2x~ipad.png"},
    {"res/ios/splash/Default-Landscape-736h.png": "platforms/ios/ТРЭК 2018/Images.xcassets/LaunchImage.launchimage/Default-Landscape-736h.png"},
    {"res/ios/splash/Default-Landscape~ipad.png": "platforms/ios/ТРЭК 2018/Images.xcassets/LaunchImage.launchimage/Default-Landscape~ipad.png"},
    {"res/ios/splash/Default-Portrait@2x~ipad.png": "platforms/ios/ТРЭК 2018/Images.xcassets/LaunchImage.launchimage/Default-Portrait@2x~ipad.png"},
    {"res/ios/splash/Default-Portrait~ipad.png": "platforms/ios/ТРЭК 2018/Images.xcassets/LaunchImage.launchimage/Default-Portrait~ipad.png"},
    {"res/ios/splash/Default~iphone.png": "platforms/ios/ТРЭК 2018/Images.xcassets/LaunchImage.launchimage/Default~iphone.png"}//,
    //{"res/icon.png": "platforms/ios/YourAppName/Resources/icons/icon-72.png"}
];

var fs = require('fs');
var path = require('path');

// no need to configure below
var rootDir = process.argv[2];

filesToCopy.forEach(function(obj) {
    Object.keys(obj).forEach(function(key) {
        var val = obj[key];
        var srcFile = path.join(rootDir, key);
        var dstFile = path.join(rootDir, val);
        console.log("copying "+srcFile+" to "+dstFile);
        var dstDir = path.dirname(dstFile);
        if (fs.existsSync(srcFile) && fs.existsSync(dstDir)) {
            fs.createReadStream(srcFile).pipe(
                fs.createWriteStream(dstFile)
            );
        } else {
            if (!fs.existsSync(srcFile)) {
                console.error('failed: no srcFile!');
            } else {
                //console.error('failed: no dstDir!');
            }
        }
    });
});
